import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import Appbar from "./view/containers/Appbar";
import LoginPage from "./view/pages/LoginPage";
import PlayerPage from "./view/pages/PlayerPage";
import TeamPage from "./view/pages/TeamPage";
import MatchPage from "./view/pages/MatchPage";
import UserPage from "./view/pages/UserPage";
import PersonPage from "./view/pages/PersonPage";
import AssociationPage from "./view/pages/AssociationPage";
import AddressPage from "./view/pages/AddressPage";
import GoalPage from "./view/pages/GoalPage";
import LocationPage from "./view/pages/LocationPage";
import ProfilePage from "./view/pages/ProfilePage";
import OwnerPage from "./view/pages/OwnerPage";
import CoachPage from "./view/pages/CoachPage";
import SeasonPage from "./view/pages/SeasonPage";
import FormDisplayer from "./view/components/utils/FormDisplayer";

@inject("rootView", "coachStore", "ownerStore", "matchStore", "playerStore")
@observer
class App extends Component<any, any> {
  render() {
    let {
      rootView: { currentPage }
    } = this.props;

    return (
      <div>
        {currentPage !== "loginPage" && <Appbar />}
        {currentPage === "loginPage" && <LoginPage />}
        {currentPage === "playerPage" && <PlayerPage />}
        {currentPage === "teamPage" && <TeamPage />}
        {currentPage === "matchPage" && <MatchPage />}
        {currentPage === "userPage" && <UserPage />}
        {currentPage === "personPage" && <PersonPage />}
        {currentPage === "associationPage" && <AssociationPage />}
        {currentPage === "addressPage" && <AddressPage />}
        {currentPage === "goalPage" && <GoalPage />}
        {currentPage === "locationPage" && <LocationPage />}
        {currentPage === "profilePage" && <ProfilePage />}
        {currentPage === "ownerPage" && <OwnerPage />}
        {currentPage === "coachPage" && <CoachPage />}
        {currentPage === "seasonPage" && <SeasonPage />}

      <FormDisplayer/>
      </div>
    );
  }
}

export default App;
