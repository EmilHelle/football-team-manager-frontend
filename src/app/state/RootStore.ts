import Player from "../model/Player";
import Match from "../model/Match";
import Team from "../model/Team";
import User from "../model/User";
import Person from "../model/Person";
import Goal from "../model/Goal";
import Association from "../model/Association";
import Address from "../model/Address";
import Owner from "../model/Owner";
import Location from "../model/Location";
import Coach from "../model/Coach";
import Season from "../model/Season";
import UserStore from "./stores/UserStore";
import AuthStore from "./stores/AuthStore";
import ResultStore from "./stores/ResultStore";
import RootView from "./stores/RootView";
import Store from "./stores/Store";

class RootStore {
	playerStore: Store<Player>;
	matchStore: Store<Match>;
	teamStore: Store<Team>;
	personStore: Store<Person>;
	goalStore: Store<Goal>;
	associationStore: Store<Association>;
	addressStore: Store<Address>;
	locationStore: Store<Location>;
	ownerStore: Store<Owner>;
	coachStore: Store<Coach>;
	seasonStore: Store<Season>;
	userStore: UserStore;
	authStore: AuthStore;
	resultStore: ResultStore;
	rootView: RootView;

	constructor() {
		this.playerStore = new Store<Player>(
			this,
			new Player(new Person(""), new Team(""), "", null, null),
			null
		);
		this.matchStore = new Store<Match>(
			this,
			new Match(new Date(), new Team(""), new Team("")),
			null
		);
		this.teamStore = new Store<Team>(this, new Team(""), null);
		this.personStore = new Store<Person>(this, new Person(""), null);
		this.goalStore = new Store<Goal>(this, new Goal(""), null);
		this.associationStore = new Store<Association>(this, new Association("", ""), null);
		this.addressStore = new Store<Address>(this, new Address(0, "", "", ""), null);
		this.locationStore = new Store<Location>(this, new Location(null, ""), null);
		this.ownerStore = new Store<Owner>(
			this,
			new Owner(new Person("", "", new Date(), new Address(0, "", "", "", "", ""))),
			null
		);
		this.coachStore = new Store<Coach>(
			this,
			new Coach(new Person("", "", new Date(), new Address(0, "", "", "", "", ""))),
			null
		);
		this.seasonStore = new Store<Season>(this, new Season(null, null, ""), null);
		this.authStore = new AuthStore(this);
		this.userStore = new UserStore(this);
		this.resultStore = new ResultStore(this);
		this.rootView = new RootView(this);
	}
}

export default RootStore;
