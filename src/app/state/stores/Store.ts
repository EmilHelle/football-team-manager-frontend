import { observable, action } from "mobx";

class Store<T> {
  @observable list: T[];
  @observable current: T;
  @observable target: T;

  private rootStore;
  constructor(rootStore, current: T, target: T) {
    this.rootStore = rootStore;
    this.list = [];
    this.current = current;
    this.target = target;
  }
  @action.bound
  setCurrent(current: T): void {
    this.current = current;
  }

  @action.bound
  setTarget(target: T): void {
    this.target = target;
  }

  @action.bound
  setList(list: T[]): void {
    this.list = list;
  }

  @action.bound
  add(entity: T): void {
    this.list.push(entity);
  }

  @action.bound
  remove(entity: T): void {
    this.list = this.list.filter(obj => obj !== entity);
  }

  scrollToBottom() {
	  window.scrollTo({top: document.body.scrollHeight+50,
		behavior: 'smooth'     });
  }
}

export default Store;
