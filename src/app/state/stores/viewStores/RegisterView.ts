import { observable, action } from "mobx";

class RegisterView {
	@observable currentView: string;
	@observable currentUserFormOpen: boolean;
	@observable targetUserFormOpen: boolean;

	private rootStore;
	constructor(rootStore) {
		this.rootStore = rootStore;
		this.currentView = "";
		this.currentUserFormOpen = false;
		this.targetUserFormOpen = false;
	}

	@action.bound
	setCurrentRegisterFormOpen(b: boolean): void {
		this.currentUserFormOpen = b;
	}

	@action.bound
	setTargetRegisterFormOpen(b: boolean): void {
		this.targetUserFormOpen = b;
	}
}

export default RegisterView;
