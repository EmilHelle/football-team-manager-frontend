import { observable, action } from 'mobx';

class LoginView {
  @observable currentView: string;
  @observable currentLoginFormOpen: boolean;
  @observable targetLoginFormOpen: boolean;
  @observable currentRegisterFormOpen: boolean;
  @observable targetRegisterFormOpen: boolean;

  private rootStore;
  constructor(rootStore) {
    this.rootStore = rootStore;
    this.currentView = '';
    this.currentLoginFormOpen = false;
    this.targetLoginFormOpen = false;
    this.currentRegisterFormOpen = false;
    this.targetRegisterFormOpen = false;
  }

  @action.bound
  setCurrentLoginFormOpen(b:boolean):void{
    this.currentLoginFormOpen = b;
  }

  @action.bound
  setTargetLoginFormOpen(b:boolean):void{
    this.targetLoginFormOpen = b;
  }

  @action.bound
  setCurrentRegisterFormOpen(b:boolean):void{
    this.currentRegisterFormOpen = b;
  }

  @action.bound
  setTargetRegisterFormOpen(b:boolean):void{
    this.targetRegisterFormOpen = b;
  }

}

export default LoginView;