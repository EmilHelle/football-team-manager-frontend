import { observable, action } from "mobx";

class ViewStore {
  @observable currentView: string;
  @observable currentFormOpen: boolean;
  @observable targetFormOpen: boolean;

  constructor(
    currentView?: string,
    currentFormOpen?: boolean,
    targetFormOpen?: boolean
  ) {
    this.currentView = currentView;
    this.currentFormOpen = currentFormOpen;
    this.targetFormOpen = targetFormOpen;
  }

  @action.bound
  setCurrentView(view: string): void {
    this.currentView = view;
  }

  @action.bound
  setCurrentFormOpen(b: boolean): void {
    this.currentFormOpen = b;
  }

  @action.bound
  setTargetFormOpen(b: boolean): void {
    this.targetFormOpen = b;
  }
}

export default ViewStore;
