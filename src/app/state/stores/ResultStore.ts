import { observable, action, computed, autorun } from "mobx";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Result from "../../model/Result";
import User from "../../model/User";

class ResultStore {
	@observable results: Result[];
	@observable currentResult: Result;
	@observable targetResult: Result;
	@observable loggedInUser: { user: User; token: string };

	private rootStore;
	constructor(rootStore) {
		this.rootStore = rootStore;
		this.results = [];
		this.currentResult = new Result(null, null, 0, "");
		this.targetResult = null;
		this.loggedInUser = this.rootStore.authStore.loggedInUser;
	}

	@action.bound
	async getResults() {
		var returnedResults = await databaseAccessor.getAll("results", this.loggedInUser.token);
		for (let index = 0; index < returnedResults.length; index++) {
			const result = returnedResults[index];
			console.log("Result: ", result);
			this.results.push(new Result(result.match, result.team, result.score, result.matchResult));
		}
    }
    
    @action.bound
	async getResult(teamId, matchId) {
        try {
            var data = await databaseAccessor.getSingle("results", teamId, this.loggedInUser.token, matchId);
            if (data) {
                this.targetResult = new Result(data.match, data.team, data.score, data.result);
            } else {
                throw "something whent wrong when getting result";
            }
        } catch (error) {
            console.log(error);
        }
    }

	@action.bound
    setCurrentResult(result: Result): void {
		this.currentResult = result;
	}

	@action.bound
	setTargetResult(result: Result): void {
		this.targetResult = result;
	}

	@action.bound
	addResult(result: Result) {
		this.results.push(result);
	}

	@action.bound
	editResult(result: Result): void {
		this.targetResult = result;
		this.rootStore.rootView.resultView.setTargetResultFormOpen(true);
	}

	@action.bound
	updateTargetResult(): void {
		this.rootStore.rootView.resultView.setTargetResultFormOpen(true);
		databaseAccessor.update("results", this.targetResult.resultToJson(), this.targetResult.id, this.loggedInUser.token);
	}

	@action.bound
	submitCurrentResult(): void {
		databaseAccessor.create("results", this.currentResult.resultToJson(), this.loggedInUser.token);
	}

	@action.bound
	deleteTargetResult(): void {
		databaseAccessor.delete("results", this.targetResult.resultToJson(), this.targetResult.id, this.loggedInUser.token);
	}
}

export default ResultStore;
