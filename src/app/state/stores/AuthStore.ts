import { observable, action, computed, autorun } from "mobx";
import User from "../../model/User";
import authenticationAccessor from "../../backend/auth/AuthenticationAccessor";

class AuthStore {
	@observable loggedInUser: { user: User; token: string };

	private rootStore;
	constructor(rootStore) {
		this.rootStore = rootStore;
		this.loggedInUser = { user: new User("", "", ""), token: "" };
	}
	adminToJson() {
		return {
			userName: this.loggedInUser.user.name,
			email: this.loggedInUser.user.email,
			password: this.loggedInUser.user.password,
			isAdmin: this.loggedInUser.user.isAdmin,
			id: this.loggedInUser.user.id
		};
	}
	@action.bound
	async register(user: User) {
		try {
			await authenticationAccessor.register(user.userToJson()).then(response => {
				if (response.data.succeeded) {
					console.log("Registration successfull");
					return null;
				} else {
					throw response.data.errors;
				}
			});
			//Mapping all errors to array for use in frontend, and printing to console for easy debugging
		} catch (error) {
			let errors: string[] = Array();
			error.forEach(element => {
				errors.push(element.description);
				console.log("Registration error: ", element.description);
			});
			return errors;
		}
	}

	@action.bound
	async login(user: User) {
		try {
			await authenticationAccessor.loginUser(user.userLoginCredentialsToJson()).then(response => {
				let responseToken = response.data.token;
				let responseUser = response.data.user;
				if (responseToken !== undefined) {
					this.loggedInUser = { user: responseUser, token: responseToken };
					console.log("Login successfull");
				}
			});
		} catch (error) {
			console.log("Login error: ", error);
		}
	}

	@action.bound
	logout(user: User): void {}

	@computed
	get loggedIn(): boolean {
		if (this.loggedInUser.token !== "" && this.loggedInUser.token !== undefined) {
			return true;
		}
	}
}

export default AuthStore;
