import { observable, action } from "mobx";
import Theme from "../../model/Theme";
import ViewStore from "./viewStores/ViewStore";
import LoginView from "./viewStores/LoginView";

class RootView {
	@observable currentPage: string;

	playerView: ViewStore;
	teamView: ViewStore;
	matchView: ViewStore;
	addressView: ViewStore;
	personView: ViewStore;
	associationView: ViewStore;
	locationView: ViewStore;
	goalView: ViewStore;
	userView: ViewStore;
	ownerView: ViewStore;
	coachView: ViewStore;
	seasonView: ViewStore;
	resultView: ViewStore;
	loginView :LoginView;
	theme: Theme;

	private rootStore;
	constructor(rootStore) {
		this.rootStore = rootStore;
		this.currentPage = "loginPage";
		this.playerView = new ViewStore();
		this.teamView = new ViewStore();
		this.matchView = new ViewStore();
		this.addressView = new ViewStore();
		this.personView = new ViewStore();
		this.associationView = new ViewStore();
		this.locationView = new ViewStore();
		this.goalView = new ViewStore();
		this.userView = new ViewStore();
		this.ownerView = new ViewStore();
		this.coachView = new ViewStore();
		this.seasonView = new ViewStore();
		this.resultView = new ViewStore();
		this.loginView = new LoginView(this.rootStore);
		this.theme = new Theme("dark");
	}

	@action.bound
	setCurrentPage(page: string): void {
		this.currentPage = page;
	}
}

export default RootView;
