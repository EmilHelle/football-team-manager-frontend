import { observable, action, computed, autorun } from "mobx";
import User from "../../model/User";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import authenticationAccessor from "../../backend/auth/AuthenticationAccessor";

class UserStore {
	@observable users: User[];
	@observable currentUser: User;
	@observable targetUser: User;
	@observable loggedInUser: { user: User; token: string };

	private rootStore;
	constructor(rootStore) {
		this.rootStore = rootStore;
		this.users = [];
		this.currentUser = new User("", "", "", null);
		this.targetUser = new User("", "", "", null);
		console.log("UserStore constructor=> loggedInUser:", this.rootStore.authStore.loggedInUser);
		this.loggedInUser = this.rootStore.authStore.loggedInUser;
	}

	@action.bound
	setCurrentUser(user: User): void {
		this.currentUser = user;
	}

	@action.bound
	setTargetUser(user: User): void {
		this.targetUser = user;
	}

	@action.bound
	async getUsers() {
		var returnedUsers = await databaseAccessor.getAll("register/debug", this.loggedInUser.token);
		for (let index = 0; index < returnedUsers.length; index++) {
			const user = returnedUsers[index];
			console.log("user", user);
			this.users.push(new User(user.userName, user.email, user.password, user.isAdmin, user.id));
		}
	}

	@computed
	get allUsers(): User[] {
		return this.users;
	}

	@action.bound
	editUser(user: User): void {
		this.targetUser = user;
		console.log("editUser=> targetUser:", this.targetUser);
		this.rootStore.rootView.userView.setTargetFormOpen(true);
	}

	@action.bound
	updateTargetUser(): void {
		databaseAccessor.update("register/debug", this.targetUser.userToJson(), this.targetUser.id, this.loggedInUser.token);
		//close modal
		this.rootStore.rootView.userView.setTargetFormOpen(false);
	}

	@action.bound
	deleteTargetUser(): void {
		databaseAccessor.delete("register/debug", this.targetUser.userToJson(), this.targetUser.id, this.loggedInUser.token);
		//close modal
		this.rootStore.rootView.userView.setTargetFormOpen(false);
	}

	@action.bound
	makeAdmin(): void {
		console.log("makeAdmin=> loggedInUser:", this.rootStore.authStore.loggedInUser);
		this.targetUser.setType(true);
		authenticationAccessor.elevateToAdmin(
			this.rootStore.authStore.adminToJson(),
			this.targetUser.userToJson(),
			this.loggedInUser.token
		);
	}
}

export default UserStore;
