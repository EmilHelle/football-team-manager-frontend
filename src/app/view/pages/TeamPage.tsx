import React from "react";
import { inject, observer } from "mobx-react";
import TeamContainer from "../containers/TeamContainer";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import TeamForm from "../components/team/forms/TeamForm";

@inject("teamStore", "rootView", "userStore", "authStore")
@observer
class TeamPage extends React.Component<any, any> {
  render() {
    let {
      teamStore: { current, target }
    } = this.props;
    let currentTeam = current;
    let targetTeam = target;
    let {
      rootView: {
        teamView: { currentFormOpen, setCurrentFormOpen, targetFormOpen }
      }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;
    let {
      authStore: {loggedInUser }
    } = this.props;

    return (
      <div style={{margin: "20px"}}>
        <Typography 
        variant="h3" 
        style={{ color: theme.currentTheme.textMain, margin: "15px 10px 5px 10px"}}>
          Teams
        </Typography>
        <TeamContainer />
        {loggedInUser.user.isAdmin &&
        <Fab
          style={{ 
            color: theme.currentTheme.secondaryContrast, 
            backgroundColor: theme.currentTheme.main, 
            position: "fixed", 
            bottom: "3%", 
            right: "2%" }}
          onClick={() => setCurrentFormOpen(true)}
        >
          <AddIcon />
        </Fab>
        }
      </div>
    );
  }
}

export default TeamPage;
