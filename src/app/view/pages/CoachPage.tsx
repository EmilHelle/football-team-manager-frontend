import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import CoachForm from "../components/coach/forms/CoachForm";
import CoachContainer from "../containers/CoachContainer";

@inject("coachStore", "rootView", "userStore","authStore")
@observer
class CoachPage extends React.Component<any, any> {
  render() {
    let {
      rootView: {
        coachView: { setCurrentFormOpen }
      }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
			authStore: { loggedInUser }
		} = this.props;

    return (
      <div style={{ margin: "20px" }}>
        <Typography
          variant="h3"
          style={{
            color: theme.currentTheme.textMain,
            margin: "15px 10px 5px 10px"
          }}
        >
          Coaches
        </Typography>
        <CoachContainer />
        {loggedInUser.user.isAdmin && (
					<Fab
            style={{ 
              color: theme.currentTheme.secondaryContrast, 
              backgroundColor: theme.currentTheme.main, 
              position: "fixed", 
              bottom: "3%", 
              right: "2%" }}
						onClick={() => setCurrentFormOpen(true)}
					>
						<AddIcon />
					</Fab>
				)}
      </div>
    );
  }
}

export default CoachPage;
