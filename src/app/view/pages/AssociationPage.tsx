import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import AssociationForm from "../components/association/forms/AssociationForm";
import AssociationContainer from "../containers/AssociationContainer";

@inject("associationStore", "rootView", "userStore", "authStore")
@observer
class AssociationPage extends React.Component<any, any> {
  render() {
    let {
      rootView: {
        associationView: { setCurrentFormOpen }
      }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    return (
      <div style={{ margin: "20px" }}>
        <Typography
          variant="h3"
          style={{
            color: theme.currentTheme.textMain,
            margin: "15px 10px 5px 10px"
          }}
        >
          Associations
        </Typography>
        <AssociationContainer />
        {loggedInUser.user.isAdmin && (
          <Fab
            color="primary"
            style={{
              color: theme.currentTheme.secondaryContrast,
              backgroundColor: theme.currentTheme.main,
              position: "fixed",
              bottom: "3%",
              right: "2%"
            }}
            onClick={() => setCurrentFormOpen(true)}
          >
            <AddIcon />
          </Fab>
        )}
      </div>
    );
  }
}

export default AssociationPage;
