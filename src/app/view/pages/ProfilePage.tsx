import React from "react";
import { inject, observer } from "mobx-react";
import ProfileContainer from "../containers/ProfileContainer";

@inject("authStore")
@observer
class ProfilePage extends React.Component<any, any> {
	render() {
		let {
			authStore: { loggedInUser }
		} = this.props;
		return (
			<div style={{ margin: "20px" }}>
				{loggedInUser.token &&
					<ProfileContainer />
				}
			</div>
		);
	}
}

export default ProfilePage;
