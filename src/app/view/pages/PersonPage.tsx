import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import PersonForm from "../components/person/forms/PersonForm";
import PersonContainer from "../containers/PersonContainer";

@inject("personStore", "rootView", "userStore", "authStore")
@observer
class PersonPage extends React.Component<any, any> {
  render() {
    let currentPerson = this.props.personStore.current;
    let targetPerson = this.props.personStore.target;
    let {
      rootView: {
        personView: { currentFormOpen, setCurrentFormOpen, targetFormOpen }
      }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
			authStore: { loggedInUser }
		} = this.props;

    return (
      <div style={{ margin: "20px" }}>
        <Typography
          variant="h3"
          style={{
            color: theme.currentTheme.textMain,
            margin: "15px 10px 5px 10px"
          }}
        >
          People
        </Typography>
        <PersonContainer />
        {loggedInUser.user.isAdmin && (
					<Fab
						style={{ 
              color: theme.currentTheme.secondaryContrast, 
              backgroundColor: theme.currentTheme.main, 
              position: "fixed", 
              bottom: "3%", 
              right: "2%" }}
						onClick={() => setCurrentFormOpen(true)}
					>
						<AddIcon />
					</Fab>
				)}
      </div>
    );
  }
}

export default PersonPage;
