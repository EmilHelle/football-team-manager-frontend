import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import UserForm from "../components/user/forms/UserForm";
import UserContainer from "../containers/UserContainer";

@inject("userStore", "rootView", "authStore")
@observer
class UserPage extends React.Component<any, any> {
	render() {
		let {
			userStore: { currentUser, targetUser }
		} = this.props;
		let {
			rootView: {
				userView: { currentFormOpen, setCurrentFormOpen, targetFormOpen }
			}
		} = this.props;
		let {
			rootView: { theme }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;

		return (
			<div style={{ margin: "20px" }}>
				<Typography
					variant="h3"
					style={{
						color: theme.currentTheme.textMain,
						margin: "15px 10px 5px 10px"
					}}
				>
					Users
				</Typography>
				<UserContainer />
				{currentFormOpen && <UserForm user={currentUser} />}
				{targetFormOpen && <UserForm user={targetUser} />}
			</div>
		);
	}
}

export default UserPage;
