import React from "react";
import { inject, observer } from "mobx-react";
import RegisterForm from "../components/auth/forms/RegisterForm";
import LoginForm from "../components/auth/forms/LoginForm";
import { Button, TextField, InputAdornment } from "@material-ui/core";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Lock from "@material-ui/icons/Lock";
import User from "../../model/User";

@inject("rootView", "userStore", "authStore")
@observer
class LoginPage extends React.Component<any, any> {

	handleLoginAttempt = () => {
		let {
			authStore: { loggedInUser }
		} = this.props;
		let {
			userStore: { setCurrentUser}
		} = this.props;
		let {
			rootView: { setCurrentPage }
		} = this.props;

		setCurrentUser(new User ("", "", ""));

		if (loggedInUser.token !== "" && loggedInUser.token !== undefined) {
			setCurrentPage("matchPage");
		} else {
			alert("Invalid login");
		}		
	}

	render() {
		let {
			rootView: { 
				theme, 
				setCurrentPage}
		} = this.props;
		let {
			rootView: {
				loginView: { 
					currentLoginFormOpen, 
					setCurrentLoginFormOpen, 
					currentRegisterFormOpen, 
					setCurrentRegisterFormOpen }
			}
		} = this.props;
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { login }
		} = this.props;
		 
		return (
			<div style={{margin: "20px", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", height: "700px"}}>
				<div style={{display: "flex", flexDirection: "column", alignSelf: "center", alignItems: "center"}}>
				<h1 style={{color: theme.currentTheme.textMain, fontFamily: "calibri"}}>FOOTBALL TEAM MANAGER</h1>
				<div style={{padding: "10px", display:"flex", alignItems: "center",  flexDirection: "column"}}>
					<div style={{padding: "15px 30px", borderRadius: "40px", backgroundColor: "white", width: "250px", margin: "5px 0"}}>
						<TextField  
						style={{width: "inherit"}}
						value={currentUser.name}
						onChange={currentUser.changeName}
						color="white"
						label="Username"
						InputProps={{
						startAdornment: (
							<InputAdornment position="start">
							<AccountCircle />
							</InputAdornment>
						),
						}}
						/>
					</div>
					<div style={{padding: "15px 30px", borderRadius: "40px", backgroundColor: "white", width: "250px", margin: "10px 0"}}>
						<TextField  
							style={{width: "inherit"}}
							value={currentUser.password}
							onChange={currentUser.changePassword}
							label="Password"
							type="password"
							InputProps={{
							startAdornment: (
								<InputAdornment position="start">
								<Lock />
								</InputAdornment>
							),
							}}
						/>
					</div>
					<div style={{ display: "flex", justifyContent: "center", width: "300px", margin: "5px 0"}}>
						<Button 
						style={{ borderRadius: "10px", width: "inherit"}} 
						variant="contained" 
						color="primary"
						onClick={async () => { await login(currentUser); this.handleLoginAttempt();}}>
							Login
						</Button>
					</div>
					<div style={{  width: "300px", margin: "25px 0 5px"}}>
						<Button 
						style={{ borderRadius: "10px", width: "inherit" }} 
						variant="contained" 
						color="secondary"
						onClick={() => setCurrentPage("matchPage")}>
							Continue as guest
						</Button>
					</div>
					<div style={{ width: "300px", margin: "5px 0"}}>
						<Button 
						style={{ borderRadius: "10px", width: "inherit" }} 
						variant="contained"
						onClick={() => setCurrentRegisterFormOpen(true)}>
							Register
						</Button>
					</div>
				</div>
				</div>
				{currentLoginFormOpen && <LoginForm/>}
				{currentRegisterFormOpen && <RegisterForm/>}
			</div>
		);
	}
}

export default LoginPage;
