import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import SeasonContainer from "../containers/SeasonContainer";

@inject("seasonStore", "rootView", "userStore", "authStore")
@observer
class SeasonPage extends React.Component<any, any> {
  render() {
    let {
      rootView: {
        seasonView: { setCurrentFormOpen }
      }
    } = this.props;
    let {rootView: {theme}} = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
			authStore: { loggedInUser }
		} = this.props;
    return (
      <div style={{ margin: "20px" }}>
        <Typography
          variant="h3"
          style={{
            color: theme.currentTheme.textMain,
            margin: "15px 10px 5px 10px"
          }}
        >
          Seasons
        </Typography>
        <SeasonContainer />
      	{loggedInUser.user.isAdmin && (
					<Fab
						style={{ 
              color: theme.currentTheme.secondaryContrast, 
              backgroundColor: theme.currentTheme.main, 
              position: "fixed", 
              bottom: "3%", 
              right: "2%" }}
          				onClick={() => setCurrentFormOpen(true)}
					>
						<AddIcon />
					</Fab>
				)}
      </div>
    );
  }
}

export default SeasonPage;
