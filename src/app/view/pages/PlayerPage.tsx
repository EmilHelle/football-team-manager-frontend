import React from "react";
import { inject, observer } from "mobx-react";
import PlayerContainer from "../containers/PlayerContainer";
import Typography from "@material-ui/core/Typography";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import PlayerForm from "../components/player/forms/PlayerForm";

@inject("playerStore", "rootView", "userStore", "authStore")
@observer
class PlayerPage extends React.Component<any, any> {
  render() {
    let {
      playerStore: { current, target }
    } = this.props;
    let currentPlayer = current;
    let targetPlayer = target;
    let {
      rootView: {
        playerView: { currentFormOpen, setCurrentFormOpen, targetFormOpen }
      }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;
    let {
      authStore: {loggedInUser }
    } = this.props;

    return (
      <div style={{ margin: "20px" }}>
        <Typography
          variant="h3"
          style={{
            color: theme.currentTheme.textMain,
            margin: "15px 10px 5px 10px"
          }}
        >
          Players
        </Typography>
        <PlayerContainer />
       {loggedInUser.user.isAdmin &&
        <Fab
          style={{ 
            color: theme.currentTheme.secondaryContrast, 
            backgroundColor: theme.currentTheme.main, 
            position: "fixed", 
            bottom: "3%", 
            right: "2%" }}
          onClick={() => setCurrentFormOpen(true)}
        >
          <AddIcon />
        </Fab>
        }
      </div>
    );
  }
}

export default PlayerPage;
