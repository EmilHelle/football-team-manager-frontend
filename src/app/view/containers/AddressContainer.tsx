import React from "react";
import { inject, observer } from "mobx-react";
import AddressCard from "../components/address/AddressCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Address from "../../model/Address";

@inject("addressStore", "rootView", "authStore")
@observer
class AddressContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.addressStore.setList([]);
		this.getAddresses();
	}

  async getAddresses() {
    var returnedAddresses = await databaseAccessor.getAll("addresses", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedAddresses.length; index++) {
      const address = returnedAddresses[index];
      this.props.addressStore.add(
        new Address(
          address.postalCode,
          address.city,
          address.country,
          address.addressLine1,
          address.addressLine2,
          address.addressLine3,
          address.id
        )
      );
    }
  }

	render() {
		let addresses = this.props.addressStore.list;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<div>
				<Typography
					variant="h5"
					style={{
						margin: "0 10px 15px 10px",
						color: theme.currentTheme.textMain
					}}
				>
					<i>
						{addresses.length} registered {addresses.length === 1 ? "address" : "addresses"}
					</i>
				</Typography>
				{addresses.length === 0 && <Loading />}
				{addresses && addresses.map(address => <AddressCard address={address} />)}
			</div>
		);
	}
}

export default AddressContainer;
