import React from "react";
import { inject, observer } from "mobx-react";
import OwnerCard from "../components/owner/OwnerCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Owner from "../../model/Owner";
import Address from "../../model/Address";
import Person from "../../model/Person";

@inject("ownerStore", "rootView", "authStore")
@observer
class OwnerContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.ownerStore.setList([]);
		this.getOwners();
	  }

	  async getOwners() {
		var returnedOwners = await databaseAccessor.getAll("owners", this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedOwners.length; index++) {
		  const owner = returnedOwners[index];
	
		  this.props.ownerStore.add(
			new Owner(
			  new Person(
				owner.person.firstName,
				owner.person.lastName,
				owner.person.dateOfBirth,
			  new Address(
				owner.person.address.postalCode,
				owner.person.address.city,
				owner.person.address.country,
				owner.person.address.addressLine1,
				owner.person.address.addressLine2,
				owner.person.address.addressLine3,
				owner.person.address.id
			  ),
			  owner.person.id
			),
			owner.id
			)
		  );
		}
	  }
	  
	render() {
		let owners = this.props.ownerStore.list;
		let {
			rootView: { theme }
		  } = this.props;
		return (
			<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
					<i>
						{owners.length} registered {owners.length === 1 ? "owner" : "owners"}
					</i>
				</Typography>
				{owners.length === 0 && <Loading />}
				{owners.map(owner => (
					<OwnerCard owner={owner} />
				))}
			</div>
		);
	}
}

export default OwnerContainer;
