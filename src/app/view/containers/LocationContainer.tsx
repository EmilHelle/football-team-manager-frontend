import React from "react";
import { inject, observer } from "mobx-react";
import LocationCard from "../components/location/LocationCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Address from "../../model/Address";
import Location from "../../model/Location";

@inject("locationStore","rootView", "authStore")
@observer
class LocationContainer extends React.Component<any, any> {
  componentDidMount() {
		this.props.locationStore.setList([]);
		this.getLocations();
    }
    
    async getLocations() {
      var returnedLocations = await databaseAccessor.getAll('locations', this.props.authStore.loggedInUser.token);
      for (let index = 0; index < returnedLocations.length; index++) {
        const location = returnedLocations[index];

        this.props.locationStore.add(
          new Location(
            new Address(
              location.address.postalCode,
              location.address.city,
              location.address.country,
              location.address.addressLine1,
              location.address.addressLine2,
              location.address.addressLine3,
              location.address.id
            ),
            location.name,
            location.description,
            location.id
          )
        );
      }
    }

  render() {
    let locations = this.props.locationStore.list;
    let {
      rootView: { theme }
    } = this.props;
    return (
      <div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
            <i>{locations.length} registered {locations.length===1 ? "location" : "locations"}</i>
        </Typography>
        {locations.length === 0 && <Loading/>}
        {locations.map(location => (
         <LocationCard location={location}/>
        ))}
      </div>
    );
  }
}

export default LocationContainer;
