import React from "react";
import { inject, observer } from "mobx-react";
import TeamCard from "../components/team/TeamCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Team from "../../model/Team";
import Owner from "../../model/Owner";
import Address from "../../model/Address";
import Coach from "../../model/Coach";
import Person from "../../model/Person";
import Association from "../../model/Association";
import Location from "../../model/Location";

@inject("teamStore", "rootView", "authStore")
@observer
class TeamContainer extends React.Component<any, any> {
  componentDidMount() {
    this.props.teamStore.setList([]);
    this.getTeams();
  }

  async getTeams() {
		var returnedTeams = await databaseAccessor.getAll("teams", this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedTeams.length; index++) {
			const team = returnedTeams[index];
			this.props.teamStore.add(
				new Team(
					team.name,
					new Owner(
						new Person(
							team.owner.person.firstName,
							team.owner.person.lastName,
							new Date(team.owner.person.dateOfBirth),
							new Address(
								team.owner.person.address.postalCode,
								team.owner.person.address.city,
								team.owner.person.address.country,
								team.owner.person.address.addressLine1,
								team.owner.person.address.addressLine2,
								team.owner.person.address.addressLine3,
								team.owner.person.address.id
							)
						),
						team.owner.id
					),
					new Coach(
						new Person(
							team.coach.person.firstName,
							team.coach.person.lastName,
							new Date(team.coach.person.dateOfBirth),
							new Address(
								team.coach.person.address.postalCode,
								team.coach.person.address.city,
								team.coach.person.address.country,
								team.coach.person.address.addressLine1,
								team.coach.person.address.addressLine2,
								team.coach.person.address.addressLine3,
								team.coach.person.address.id
							)
						),
						team.coach.id
					),
					new Association(team.association.name, team.association.description, team.association.id),
					new Location(new Address(team.location.address.postalCode, team.location.address.city, team.location.address.country, team.location.address.addressLine1, team.location.address.addressLine2, team.location.address.addressLine3), team.location.name, team.location.description, team.location.id),
					null,
					team.id
				)
			);
		}
	}

  render() {
    let {
      teamStore: { list }
    } = this.props;
    let teams = list;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <div>
        <Typography
          variant="h5"
          style={{
            margin: "0 10px 15px 10px",
            color: theme.currentTheme.textMain
          }}
        >
          <i>
            {teams.length} registered {teams.length === 1 ? "team" : "teams"}
          </i>
        </Typography>
        {teams.length === 0 && <Loading />}
        {teams.map(team => (
          <TeamCard team={team} />
        ))}
      </div>
    );
  }
}

export default TeamContainer;
