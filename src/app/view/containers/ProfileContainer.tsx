import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

@inject("rootView", "authStore")
@observer
class ProfileContainer extends React.Component<any, any> {

	goToApi() {
		window.open("https://gitlab.com/stianfa/footballteammanagerapi/blob/master/README.md");
	}
	render() {
		let {
			rootView: { theme }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;
		return (
			<div style={{ height: "50%", width: "60%", color: theme.currentTheme.textMain}}>
				<Paper style={{ 
					padding: "15px",
					color: theme.currentTheme.textMain,
					backgroundColor: theme.currentTheme.mainLight, 
					minHeight: "200px",
					display:"flex", 
					flexDirection: "column",
					justifyContent: "space-between"}}>
					<div style={{}}>
						<Typography variant="h4">
							Hello {loggedInUser.user.userName}!
						</Typography>
						<Typography variant="body1" gutterBottom>
							<i>{loggedInUser.user.isAdmin ? "You are an admin user" : ""}</i>
						</Typography>
						<Typography variant="body1" gutterBottom>
							<b>Email: </b>{loggedInUser.user.email}
						</Typography>
					</div>
					<div>
						<Button style={{
							margin: "0 10px 0 0"}} 
							variant="contained" 
							onClick={() => (
								loggedInUser.user = null, 
								loggedInUser.token = "", 
								this.props.rootView.setCurrentPage("loginPage"))
							}>
							Logout
						</Button>
						<Button style={{margin: "0 10px 0 0"}} color="primary" variant="contained">
							Update
						</Button>
						{loggedInUser.user.isAdmin &&
							<Button style={{margin: "0 10px 0 0"}} color="secondary" variant="contained" onClick={this.goToApi}>
								Go to API-doc
							</Button>
						}
					</div>
				</Paper>
			</div>
		);
	}
}

export default ProfileContainer;
