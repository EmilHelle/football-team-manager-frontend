import React from "react";
import { inject, observer } from "mobx-react";
import SeasonCard from "../components/season/SeasonCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Season from "../../model/Season";

@inject("seasonStore", "rootView", "authStore")
@observer
class SeasonContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.seasonStore.setList([]);
		this.getSeasons();
	  }

	  async getSeasons() {
		var returnedSeasons = await databaseAccessor.getAll('seasons', this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedSeasons.length; index++) {
			const season = returnedSeasons[index];

			this.props.seasonStore.add(
				new Season(
					season.startDate,
					season.endDate,
					season.name,
					season.seasonDescription,
					season.id
				)
			);
		}
	}
	
	render() {
		let seasons = this.props.seasonStore.list;
		let {
			rootView: { theme }
		} = this.props;
		return (
			<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
          			<i>{seasons.length} registered {seasons.length===1 ? "season" : "seasons"}</i>
				</Typography>
				{seasons.length === 0 && <Loading/>}
				{seasons.map(season => (
					<SeasonCard season={season} />
				))}
			</div>
		);
	}
}

export default SeasonContainer;
