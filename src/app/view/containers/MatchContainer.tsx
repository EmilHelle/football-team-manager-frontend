import React from "react";
import { inject, observer } from "mobx-react";
import MatchCard from "../components/match/MatchCard";
import Typography from "@material-ui/core/Typography";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Loading from "../components/utils/Loading";
import Match from "../../model/Match";
import Team from "../../model/Team";
import Season from "../../model/Season";
import Location from "../../model/Location";

@inject("matchStore", "teamStore", "seasonStore", "locationStore", "rootView", "authStore")
@observer
class MatchContainer extends React.Component<any, any> {
  componentDidMount() {
    this.props.matchStore.setList([]);
    this.getMatches();
  }

  async getMatches() {
    var token = this.props.authStore.loggedInUser.token
		var returnedMatches = await databaseAccessor.getAll("matches", token);
		for (let index = 0; index < returnedMatches.length; index++) {
			const match = returnedMatches[index];
			this.props.matchStore.add(
				new Match(
					new Date(match.date),
          new Team(
            match.homeTeam.name,
            match.homeTeam.owner,
            match.homeTeam.coach,
            match.homeTeam.association,
            match.homeTeam.location,
            null,
            match.homeTeam.id
          ),
          new Team(
            match.awayTeam.name,
            match.awayTeam.owner,
            match.awayTeam.coach,
            match.awayTeam.association,
            match.awayTeam.location,
            null,
            match.awayTeam.id
          ),
					new Season(
            match.season.startDate,
            match.season.endDate,
            match.season.name,
            match.season.seasonDescription,
            match.season.id
          ),
          new Location(
            match.location.address,
            match.location.name,
            match.location.description,
            match.location.id
          )
				)
			);
		}
	}


  render() {
    let {
      matchStore: { list }
    } = this.props;
    let matches = list;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <div>
        <Typography
          variant="h5"
          style={{
            margin: "0 10px 15px 10px",
            color: theme.currentTheme.textMain
          }}
        >
          <i>
            {matches.length} registered{" "}
            {matches.length === 1 ? "match" : "matches"}
          </i>
        </Typography>
        {matches.length === 0 && <Loading />}
        {matches.map(match => (
          <MatchCard match={match} />
        ))}
      </div>
    );
  }
}

export default MatchContainer;
