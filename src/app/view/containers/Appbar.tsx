import React from "react";
import { inject, observer } from "mobx-react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import SportsSoccerIcon from "@material-ui/icons/SportsSoccer";
import SettingsApplicationsIcon from "@material-ui/icons/SettingsApplications";
import { Switch, FormControlLabel, Grid } from "@material-ui/core";


@inject("rootView", "authStore")
@observer
class Appbar extends React.Component<any, any> {
	constructor(props) {
		super(props);
		this.state = {
			isMenuOpen: false,
			anchorEl: null
		};
	}

	setAnchorEl(anchor: any) {
		this.setState({ anchorEl: anchor });
		console.log("setachor=> this.state.anchor", this.state.anchorEl);
	}

	handleMenuOpen = event => {
		this.setAnchorEl(event.currentTarget);
		this.setState({ isMenuOpen: true });
	};

	handleMenuClose = () => {
		this.setState({ anchorEl: null, isMenuOpen: false });
	};

  handleMenuChoice = (choice: string) => {
    this.props.rootView.setCurrentPage(choice);
    this.handleMenuClose();
  };

  render() {
    let {
      rootView: { setCurrentPage }
    } = this.props;
    let { isMenuOpen } = this.state;
    let {
      rootView: { theme }
	} = this.props;
	let {
		authStore: { loggedIn, loggedInUser }
	} = this.props;

		return (
			<AppBar position="static" style={{ color: "white" }}>
				{console.log("Logged in as: ", loggedInUser.user)}
				<Toolbar style={{ backgroundColor: theme.currentTheme.main }}>
					<SportsSoccerIcon style={{ margin: "5px" }} />
					<Typography variant="h6" style={{ margin: "5px" }}>
						Football Manager
					</Typography>
									<Button color="inherit" onClick={() => setCurrentPage("matchPage")}>
										Matches
									</Button>
									<Button color="inherit" onClick={() => setCurrentPage("playerPage")}>
										Players
									</Button>
					<div>
						{!loggedIn && (
							<div>
								<div style={{ minWidth: "250px", marginLeft: "20px" }}>
									<Button color="inherit" onClick={() => setCurrentPage("loginPage")}>
										Login
									</Button>
								</div>
							</div>
						)}
						{loggedIn && !loggedInUser.user.isAdmin && (
							<div>
								<div style={{ minWidth: "250px", marginLeft: "20px" }}>
									<Button color="inherit" onClick={() => setCurrentPage("teamPage")}>
										Teams
									</Button>
									<IconButton style={{position: "absolute", right: "5px", top: "5px", padding: "8px"}} onClick={() => setCurrentPage("profilePage")} color="inherit">
										<AccountCircle style={{ width: "37px", height: "37px"}} />
									</IconButton>
								</div>
							</div>
						)}
						{ loggedIn && loggedInUser.user.isAdmin && (
							<Grid container spacing={3}>
								<div style={{ minWidth: "250px", marginLeft: "20px" }}>
									<Button color="inherit" onClick={() => setCurrentPage("teamPage")}>
										Teams
									</Button>
									<Button color="inherit" onClick={this.handleMenuOpen}>
										<SettingsApplicationsIcon />
										Admin
									</Button>
									{isMenuOpen && (
										<Menu
											anchorEl={this.state.anchorEl}
											anchorOrigin={{ vertical: "top", horizontal: "right" }}
											keepMounted
											transformOrigin={{ vertical: "top", horizontal: "right" }}
											open={this.state.isMenuOpen}
											onClose={this.handleMenuClose}
										>
											<MenuItem onClick={() => this.handleMenuChoice("userPage")}>Users</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("personPage")}>
												People
											</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("ownerPage")}>Owners</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("coachPage")}>
												Coaches
											</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("associationPage")}>
												Associations
											</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("addressPage")}>
												Addresses
											</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("locationPage")}>
												Locations
											</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("goalPage")}>Goals</MenuItem>
											<MenuItem onClick={() => this.handleMenuChoice("seasonPage")}>
												Seasons
											</MenuItem>
										</Menu>
									)}
									<IconButton style={{position: "absolute", right: "5px", top: "5px", padding: "8px"}} onClick={() => setCurrentPage("profilePage")} color="inherit">
										<AccountCircle style={{ width: "37px", height: "37px"}} />
									</IconButton>
								</div>
							</Grid>
						)}
					</div>
					<FormControlLabel
					control={<Switch 
						color="default" 
						checked={theme.type==="dark"} 
						onChange={theme.toggleTheme}
						size="small" />}
					label="Dark mode"
					labelPlacement="start"
					style={{position: "absolute", right: "65px"}}/>
				</Toolbar>
			</AppBar>
		);
	}
}

export default Appbar;
