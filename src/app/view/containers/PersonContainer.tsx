import React from "react";
import { inject, observer } from "mobx-react";
import PersonCard from "../components/person/PersonCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Person from "../../model/Person";
import Address from "../../model/Address";

@inject("personStore", "rootView", "authStore")
@observer
class PersonContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.personStore.setList([]);
		this.getPeople();
	}

	  async getPeople() {
		  var returnedPersons = await databaseAccessor.getAll('people', this.props.authStore.loggedInUser.token);
		  for (let index = 0; index < returnedPersons.length; index++) {
			  const person = returnedPersons[index];

			  this.props.personStore.add(
				  new Person(
					  person.firstName,
					  person.lastName,
					  new Date(person.dateOfBirth),
					  new Address(
						  person.address.postalCode,
						  person.address.city,
						  person.address.country,
						  person.address.addressLine1,
						  person.address.addressLine2,
						  person.address.addressLine3
					  ),
					  person.id
				  )
			  );
		  }
	  }

	render() {
		let persons = this.props.personStore.list;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<div>
				<Typography
					variant="h5"
					style={{
						margin: "0 10px 15px 10px",
						color: theme.currentTheme.textMain
					}}
				>
					<i>
						{persons.length} registered {persons.length === 1 ? "person" : "people"}
					</i>
				</Typography>
				{persons.length === 0 && <Loading />}
				{persons.map(person => (
					<PersonCard person={person} />
				))}
			</div>
		);
	}
}

export default PersonContainer;
