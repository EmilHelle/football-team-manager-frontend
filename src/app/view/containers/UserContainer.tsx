import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import UserCard from "../components/user/UserCard";

@inject("userStore", "rootView")
@observer
class UserContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.userStore.users = [];
		this.props.userStore.getUsers();
	}

	render() {
		let {
			userStore: { users }
		} = this.props;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
					<i>
						{users.length} registered {users.length === 1 ? "user" : "users"}{" "}
					</i>
				</Typography>
				{users.map(user => (
					<UserCard user={user} />
				))}
			</div>
		);
	}
}

export default UserContainer;
