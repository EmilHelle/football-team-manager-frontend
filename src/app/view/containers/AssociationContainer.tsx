import React from "react";
import { inject, observer } from "mobx-react";
import AssociationCard from "../components/association/AssociationCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Association from "../../model/Association";

@inject("associationStore", "rootView", "authStore")
@observer
class AssociationContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.associationStore.setList([]);
		this.getAssociations();
	  }

	async getAssociations() {
		var returnedAssociations = await databaseAccessor.getAll('associations', this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedAssociations.length; index++) {
			const association = returnedAssociations[index];
			this.props.associationStore.add(
				new Association(association.name, association.description, association.id)
			);
		}
	}

	render() {
		let associations = this.props.associationStore.list;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
					<i>
						{associations.length} registered{" "}
						{associations.length === 1 ? "association" : "associations"}
					</i>
				</Typography>
				{associations.length === 0 && <Loading/>}
				{associations.map(association => (
					<AssociationCard association={association} />
				))}
			</div>
		);
	}
}

export default AssociationContainer;
