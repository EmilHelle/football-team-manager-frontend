import React from "react";
import { inject, observer } from "mobx-react";
import PlayerCard from "../components/player/PlayerCard";
import Typography from "@material-ui/core/Typography";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Address from "../../model/Address";
import Player from "../../model/Player";
import Team from "../../model/Team";
import Owner from "../../model/Owner";
import Coach from "../../model/Coach";
import Loading from "../components/utils/Loading";
import Person from "../../model/Person";
import Association from "../../model/Association";
import Location from "../../model/Location";

@inject("playerStore", "rootView", "addressStore", "teamStore")
@observer
class PlayerContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.playerStore.setList([]);
		this.getPlayers();
	}

	async getPlayers() {
		var returnedPlayers = await databaseAccessor.getAll("players");
		for (let index = 0; index < returnedPlayers.length; index++) {
			const player = returnedPlayers[index];
			console.log("player", player);
			this.props.playerStore.add(
				new Player(
					new Person(
						player.person.firstName,
						player.person.lastName,
						player.person.dateOfBirth,
						new Address(
							player.person.address.postalCode,
							player.person.address.city,
							player.person.address.country,
							player.person.address.addressLine1,
							player.person.address.addressLine2,
							player.person.address.addressLine3,
							player.person.address.id
						),
						player.person.id
					),
					new Team(
						player.team.name,
						new Owner(
							new Person(
								player.team.owner.person.firstName,
								player.team.owner.person.lastName,
								player.team.owner.person.dateOfBirth,
								new Address(
									player.team.owner.person.address.postalCode,
									player.team.owner.person.address.city,
									player.team.owner.person.address.country,
									player.team.owner.person.address.addressLine1,
									player.team.owner.person.address.addressLine2,
									player.team.owner.person.address.addressLine3,
									player.team.owner.person.address.id
								),
								player.team.owner.person.id
							),
							player.team.owner.id
						),
						new Coach(
							new Person(
								player.team.coach.person.firstName,
								player.team.coach.person.lastName,
								player.team.coach.person.dateOfBirth,
								new Address(
									player.team.coach.person.address.postalCode,
									player.team.coach.person.address.city,
									player.team.coach.person.address.country,
									player.team.coach.person.address.addressLine1,
									player.team.coach.person.address.addressLine2,
									player.team.coach.person.address.addressLine3,
									player.team.coach.person.address.id
								),
								player.team.coach.person.id
							),
							player.team.coach.id
						),
						new Association(
							player.team.association.name,
							player.team.association.description,
							player.team.association.id
						),
						new Location(
							new Address(
								player.team.location.address.postalCode,
								player.team.location.address.city,
								player.team.location.address.country,
								player.team.location.address.addressLine1,
								player.team.location.address.addressLine2,
								player.team.location.address.addressLine3,
								player.team.location.address.id
							),
							player.team.location.name,
							player.team.location.description,
							player.team.location.id
						),
						null,
						player.team.id
					),
					player.normalPosition,
					player.number,
					player.id
				)
			);
		}
	}
	render() {
		let {
			playerStore: { list }
		} = this.props;
		let players = list;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<div>
				<Typography
					variant="h5"
					color="textSecondary"
					style={{
						margin: "0 10px 15px 10px",
						color: theme.currentTheme.textMain
					}}
				>
					<i>
						{players.length} registered {players.length === 1 ? "player" : "players"}
					</i>
				</Typography>
				{players.length === 0 && <Loading />}
				{players.map(player => (
					<PlayerCard player={player} />
				))}
			</div>
		);
	}
}

export default PlayerContainer;
