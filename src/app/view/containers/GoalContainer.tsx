import React from "react";
import { inject, observer } from "mobx-react";
import Typography from "@material-ui/core/Typography";
import GoalCard from "../components/goal/GoalCard";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Goal from "../../model/Goal";

@inject("goalStore", "rootView", "authStore")
@observer
class GoalContainer extends React.Component<any, any> {
	componentDidMount() {
		this.props.goalStore.setList([]);
		this.getGoals();
	}

	async getGoals() {
		var returnedGoals = await databaseAccessor.getAll("goaltypes", this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedGoals.length; index++) {
			const goal = returnedGoals[index];

			this.props.goalStore.add(new Goal(goal.type, goal.id));
		}
	}
	
	render() {
		let goals = this.props.goalStore.list;
		let {
			rootView: { theme }
		} = this.props;
		return (
			<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
            		<i>{goals.length} registered {goals.length===1 ? "goal" : "goals"}</i>
				</Typography>
				{goals.length === 0 && <Loading/>}
				{goals.map(goal => (
					<GoalCard goal={goal} />
				))}
			</div>
		);
	}
}

export default GoalContainer;
