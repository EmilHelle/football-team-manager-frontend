import React from "react";
import { inject, observer } from "mobx-react";
import CoachCard from "../components/coach/CoachCard";
import Typography from "@material-ui/core/Typography";
import Loading from "../components/utils/Loading";
import databaseAccessor from "../../backend/data/DatabaseAccessor";
import Coach from "../../model/Coach";
import Address from "../../model/Address";
import Person from "../../model/Person";

@inject("coachStore", "rootView", "authStore")
@observer
class CoachContainer extends React.Component<any, any> {
  componentDidMount() {
    this.props.coachStore.setList([]);
    this.getCoaches();
  }

  async getCoaches() {
    var returnedCoaches = await databaseAccessor.getAll("coaches", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedCoaches.length; index++) {
      const coach = returnedCoaches[index];

      this.props.coachStore.add(
        new Coach(
          new Person(
            coach.person.firstName,
            coach.person.lastName,
            coach.person.dateOfBirth,
          new Address(
            coach.person.address.postalCode,
            coach.person.address.city,
            coach.person.address.country,
            coach.person.address.addressLine1,
            coach.person.address.addressLine2,
            coach.person.address.addressLine3,
            coach.person.address.id
          ),
          coach.person.id
        ),
        coach.id
        )
      );
    }
  }

  render() {
    let coaches = this.props.coachStore.list;
    let {
      rootView: { theme }
    } = this.props;

    return (
    	<div>
				<Typography
					variant="h5"
					style={{
					margin: "0 10px 15px 10px",
					color: theme.currentTheme.textMain}}
				>
          <i>
						{coaches.length} registered {coaches.length === 1 ? "coach" : "coaches"}
					</i>
				</Typography>
				{coaches.length === 0 && <Loading />}
				{coaches.map(coach => (
					<CoachCard coach={coach} />
				))}
			</div>
    );
  }
}

export default CoachContainer;
