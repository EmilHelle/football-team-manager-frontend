import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import AccessibilityNewIcon from "@material-ui/icons/AccessibilityNew";
import DeleteIcon from "@material-ui/icons/Delete";
import Team from "../../../model/Team";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "teamStore", "rootView")
@observer
class TeamCard extends React.Component<any, any> {
	constructor(props) {
		super(props);
		this.state = {
			playerListExpanded: false
		};
	}

	handleChange = (event: React.ChangeEvent<{}>) => {
		this.setState({ playerListExpanded: !this.state.playerListExpanded });
	};

	editTeam = (team: Team) => {
		console.log("editTeam=>");
		this.props.teamStore.setTarget(team);
		this.props.rootView.teamView.setTargetFormOpen(true);
	};

	render() {
		let { team } = this.props;
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;
		let { playerListExpanded } = this.state;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<Card
				style={{
					backgroundColor: theme.currentTheme.mainLight,
					maxWidth: "60%",
					minWidth: "450px",
					margin: "10px"
				}}
			>
				<CardContent>
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ float: "right" }} onClick={() => this.editTeam(team)}>
							<EditIcon />
						</IconButton>
					)}
					{team.name && (
						<Typography variant="h6" gutterBottom style={{ color: theme.currentTheme.textMain }}>
							{team.name}
						</Typography>
					)}
					<Divider />
					{team.owner && (
						<Typography style={{ color: theme.currentTheme.textSecondary }} gutterBottom>
							<b>Owner: </b>
							{team.owner.person.firstName} {""}
							{team.owner.person.lastName}
						</Typography>
					)}
					{team.coach && (
						<Typography style={{ color: theme.currentTheme.textSecondary }}>
							<b>Coach: </b>
							{team.coach.person.firstName}
							{""}
							{team.coach.person.lastName}
						</Typography>
					)}
					{team.association && (
						<Typography style={{ color: theme.currentTheme.textSecondary }}>
							<b>Association: </b>
							{team.association.name}
						</Typography>
					)}
					{team.location && (
						<Typography style={{ color: theme.currentTheme.textSecondary }}>
							<b>Home Stadium: </b>
							{team.location.name}
						</Typography>
					)}
					{team.players && (
						<ExpansionPanel
							elevation={0}
							expanded={playerListExpanded}
							onChange={this.handleChange}
						>
							<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
								<Typography color="textSecondary">{team.players.length} players</Typography>
							</ExpansionPanelSummary>
							<ExpansionPanelDetails>
								<List
									style={{
										width: "100%",
										display: "flex",
										flexDirection: "column"
									}}
								>
									{team.players &&
										team.players.map(player => (
											<ListItem>
												<ListItemAvatar>
													<Avatar>
														<AccessibilityNewIcon />
													</Avatar>
												</ListItemAvatar>
												{player.firstName ? (
													<ListItemText primary={player.firstName} />
												) : (
													<ListItemText primary=".." />
												)}
												<ListItemSecondaryAction>
													<IconButton edge="end" aria-label="delete">
														<DeleteIcon />
													</IconButton>
												</ListItemSecondaryAction>
											</ListItem>
										))}
								</List>
							</ExpansionPanelDetails>
						</ExpansionPanel>
					)}
				</CardContent>
			</Card>
		);
	}
}

export default TeamCard;
