import React from "react";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { inject, observer } from "mobx-react";
import Player from "../../../../model/Player";

function not(a: number[], b: number[]) {
	var filtered = a.filter(value => b.indexOf(value) === -1);
	return filtered;
}

function intersection(a: number[], b: number[]) {
	var filtered = a.filter(value => b.indexOf(value) !== -1);
	// console.log("intersection=> filtered:", filtered);
	return filtered;
}

const customList = (players: Player[], title: string, checked: number[], handleToggle: any) => (
	<div>
		<Typography variant="h5">{title}</Typography>
		<Paper style={{ maxHeight: "200px", overflow: "auto" }}>
			<List dense component="div" role="list">
				{players.map(player => {
					const labelId = `transfer-list-item-${players.indexOf(player)}-label`;

					return (
						<ListItem
							key={players.indexOf(player)}
							role="listitem"
							button
							onClick={handleToggle(players.indexOf(player))}
						>
							<ListItemIcon>
								<Checkbox
									checked={checked.indexOf(players.indexOf(player)) !== -1}
									tabIndex={-1}
									disableRipple
									inputProps={{ "aria-labelledby": labelId }}
								/>
							</ListItemIcon>
							<ListItemText id={labelId} primary={`${player.person.firstName}`} />
						</ListItem>
					);
				})}
				<ListItem />
			</List>
		</Paper>
	</div>
);

@inject("playerStore")
@observer
class PlayerTransfer extends React.Component<any, any> {
	constructor(props) {
		super(props);
		this.state = {
			checked: []
		};
	}

	setChecked = (checked: number[]) => {
		this.setState({ checked });
	};

	get leftChecked() {
		let { checked } = this.state;
		let {
			playerStore: { players }
		} = this.props;
		let playersIndexes = [...players.map(player => players.indexOf(player))];

		return intersection(checked, playersIndexes);
	}

	get rightChecked() {
		let { checked } = this.state;
		let { team } = this.props;
		let teamPlayersIndexes = [...team.players.map(player => team.players.indexOf(player))];

		return intersection(checked, teamPlayersIndexes);
	}

	handleToggle = (value: number) => () => {
		console.log("handleToggle=> received value:", value);
		const currentIndex = this.state.checked.indexOf(value);
		console.log("handleToggle=> currentIndex in checked:", currentIndex);
		const newChecked = [...this.state.checked];

		if (currentIndex === -1) {
			newChecked.push(value);
		} else {
			newChecked.splice(currentIndex, 1);
		}
		console.log("handleToggle=> newChecked:", newChecked);
		this.setChecked(newChecked);
		console.log("handleToggle=> checked:", this.state.checked);
		console.log("handleToggle=> rightChecked:", this.rightChecked);
		console.log("handleToggle=> leftChecked:", this.leftChecked);
	};

	handleAllRight = () => {
		let { team } = this.props;
		let { playerStore } = this.props;
		let {
			playerStore: { players }
		} = this.props;
		team.setPlayers(team.players.concat(players));
		console.log("handleAllRight=> players:", players);
		playerStore.setPlayers([]);
	};

	handleCheckedRight = () => {
		let { team } = this.props;
		let { playerStore } = this.props;
		let {
			playerStore: { players }
		} = this.props;
		let playersIndexes = [...players.map(player => players.indexOf(player))];
		let notResultOfPlayers = not(playersIndexes, this.leftChecked);
		let notResultOfChecked = not(this.state.checked, this.leftChecked);
		let addedTeamPlayers = team.players.concat([...this.leftChecked.map(index => players[index])]);
		let removedPlayers = [...notResultOfPlayers.map(index => players[index])];

		team.setPlayers(addedTeamPlayers);
		playerStore.setPlayers(removedPlayers);
		this.setChecked(notResultOfChecked);

		console.log("handleCheckedRight=> players:", players);
		console.log("handleCheckedRight=> team.players:", team.players);
	};

	handleCheckedLeft = () => {
		let { team } = this.props;
		let { playerStore } = this.props;
		let {
			playerStore: { players }
		} = this.props;
		let teamPlayersIndexes = [...team.players.map(player => team.players.indexOf(player))];
		let notResultOfTeamPlayers = not(teamPlayersIndexes, this.rightChecked);
		let notResultOfChecked = not(this.state.checked, this.rightChecked);
		let addedPlayers = players.concat([...this.rightChecked.map(index => team.players[index])]);
		let removedTeamPlayers = [...notResultOfTeamPlayers.map(index => team.players[index])];

		playerStore.setPlayers(addedPlayers);
		team.setPlayers(removedTeamPlayers);
		this.setChecked(notResultOfChecked);

		console.log("handleCheckedLeft=> players:", players);
		console.log("handleCheckedLeft=> team.players:", team.players);
	};

	handleAllLeft = () => {
		let { team } = this.props;
		let { playerStore } = this.props;
		let {
			playerStore: { players }
		} = this.props;

		playerStore.setPlayers(players.concat(team.players));
		team.setPlayers([]);
		console.log("hadleAllLeft=> players:", players);
	};

	render() {
		let { team } = this.props;
		let {
			playerStore: { players }
		} = this.props;
		return (
			<Grid container spacing={2} justify="center" alignItems="center" style={{ margin: "auto" }}>
				<Grid item>{customList(players, "All players", this.leftChecked, this.handleToggle)}</Grid>
				<Grid item>
					<Grid container direction="column" alignItems="center">
						<Button
							variant="outlined"
							size="small"
							onClick={this.handleAllRight}
							disabled={players.length === 0}
							aria-label="move all right"
						>
							≫
						</Button>
						<Button
							variant="outlined"
							size="small"
							onClick={this.handleCheckedRight}
							disabled={this.leftChecked.length === 0}
							aria-label="move selected right"
						>
							&gt;
						</Button>
						<Button
							variant="outlined"
							size="small"
							onClick={this.handleCheckedLeft}
							disabled={this.rightChecked.length === 0}
							aria-label="move selected left"
						>
							&lt;
						</Button>
						<Button
							variant="outlined"
							size="small"
							onClick={this.handleAllLeft}
							disabled={team.players.length === 0}
							aria-label="move all left"
						>
							≪
						</Button>
					</Grid>
				</Grid>
				<Grid item>
					{customList(team.players, "This Teams players", this.rightChecked, this.handleToggle)}
				</Grid>
			</Grid>
		);
	}
}

export default PlayerTransfer;
