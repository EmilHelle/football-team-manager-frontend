import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import PlayerTransfer from "./PlayerTransfer";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Owner from "../../../../model/Owner";
import Address from "../../../../model/Address";
import Coach from "../../../../model/Coach";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import Person from "../../../../model/Person";
import Team from "../../../../model/Team";
import Association from "../../../../model/Association";
import Location from "../../../../model/Location";

@inject("rootView", "teamStore", "playerStore", "coachStore", "ownerStore", "associationStore", "locationStore", "authStore")
@observer
class TeamForm extends React.Component<any, any> {
  componentDidMount() {
    this.props.ownerStore.setList([]);
    this.getOwners();
    this.props.coachStore.setList([]);
    this.getCoaches();
    this.props.associationStore.setList([]);
    this.getAssociations();
    this.props.locationStore.setList([]);
    this.getLocations();
  }

  async getOwners() {
    var returnedOwners = await databaseAccessor.getAll("owners", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedOwners.length; index++) {
      const owner = returnedOwners[index];

      this.props.ownerStore.add(
        new Owner(
          new Person(
            owner.person.firstName,
            owner.person.lastName,
            owner.person.dateOfBirth,
          new Address(
            owner.person.address.postalCode,
            owner.person.address.city,
            owner.person.address.country,
            owner.person.address.addressLine1,
            owner.person.address.addressLine2,
            owner.person.address.addressLine3,
            owner.person.address.id
          ),
          owner.person.id
        ),
        owner.id
        )
      );
    }
  }

  async getCoaches() {
    var returnedCoaches = await databaseAccessor.getAll("coaches", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedCoaches.length; index++) {
      const coach = returnedCoaches[index];

      this.props.coachStore.add(
        new Coach(
          new Person(
            coach.person.firstName,
            coach.person.lastName,
            coach.person.dateOfBirth,
          new Address(
            coach.person.address.postalCode,
            coach.person.address.city,
            coach.person.address.country,
            coach.person.address.addressLine1,
            coach.person.address.addressLine2,
            coach.person.address.addressLine3,
            coach.person.address.id
          ),
          coach.person.id
        ),
        coach.id
        )
      );
    }
  }

  async getAssociations() {
		var returnedAssociations = await databaseAccessor.getAll('associations', this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedAssociations.length; index++) {
			const association = returnedAssociations[index];
			this.props.associationStore.add(
				new Association(association.name, association.description, association.id)
			);
		}
  }
  
  async getLocations() {
    var returnedLocations = await databaseAccessor.getAll('locations', this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedLocations.length; index++) {
      const location = returnedLocations[index];

      this.props.locationStore.add(
        new Location(
          new Address(
            location.address.postalCode,
            location.address.city,
            location.address.country,
            location.address.addressLine1,
            location.address.addressLine2,
            location.address.addressLine3,
            location.address.id
          ),
          location.name,
          location.description,
          location.id
        )
      );
    }
  }

  handleFormClose = () => {
    let {
      rootView: {
        teamView: { setCurrentFormOpen, setTargetFormOpen }
      }
    } = this.props;
    let {
      teamStore: { target, current, setTarget, setCurrent }
    } = this.props;
    if (target) {
      setTargetFormOpen(false);
      setTarget(null);
    }
    if (target && current) {
      setCurrentFormOpen(false);
      setCurrent(new Team(''));
    } else {
      setCurrentFormOpen(false);
      setCurrent(new Team(''));
    }
  };

  handleSelectChangeOwner = e => {
    let targetValue = e.target.value;
    var owner = this.props.ownerStore.list.find(
      owner => owner.person.firstName === targetValue
    );
    this.props.team.setOwner(owner);
  };

  handleSelectChangeCoach = e => {
    let targetValue = e.target.value;
    var coach = this.props.coachStore.list.find(
      coach => coach.person.firstName === targetValue
    );
    this.props.team.setCoach(coach);
  };

  handleSelectChangeAssociation = e => {
    let targetValue = e.target.value;
    var association = this.props.associationStore.list.find(
      association => association.name === targetValue
    );
    this.props.team.setAssociation(association);
  };

  handleSelectChangeLocation = e => {
    let targetValue = e.target.value;
    var location = this.props.locationStore.list.find(
      location => location.name === targetValue
    );
    this.props.team.setLocation(location);
  };

  handleOpenCurrentCoachForm = () => {
    this.props.rootView.coachView.setCurrentFormOpen(true);
  };

  handleOpenCurrentOwnerForm = () => {
    this.props.rootView.ownerView.setCurrentFormOpen(true);
  };

  handleOpenCurrentAssociationForm = () => {
    this.props.rootView.associationView.setCurrentFormOpen(true);
  };

  handleOpenCurrentLocationForm = () => {
    this.props.rootView.locationView.setCurrentFormOpen(true);
  };

  submitCurrentTeam = () => {
    databaseAccessor.create(
      "teams", 
      this.props.teamStore.current.teamToJson(),
      this.props.authStore.loggedInUser.token);

      this.props.teamStore.add(this.props.teamStore.current);
      this.props.teamStore.scrollToBottom();
      //close modal
      this.props.rootView.teamView.setCurrentFormOpen(false);
  };

  updateTargetTeam = () => {
    databaseAccessor.update(
      "teams",
      this.props.teamStore.target.teamToJson(),
      this.props.teamStore.target.id,
      this.props.authStore.loggedInUser.token
    );
     //close modal
	   this.props.rootView.teamView.setTargetFormOpen(false);
  };

  deleteTargetTeam = () => {
    databaseAccessor.delete(
      "teams",
      this.props.teamStore.target.teamToJson(),
      this.props.teamStore.target.id,
      this.props.authStore.loggedInUser.token
    );
    this.props.teamStore.remove(this.props.teamStore.target);
      //close modal
      this.props.rootView.teamView.setTargetFormOpen(false);
  };

  render() {
    let { team } = this.props;
    let {
      teamStore: { target }
    } = this.props;
    let targetTeam = target;
    let owners = this.props.ownerStore.list;
    let coaches = this.props.coachStore.list;
    let associations = this.props.associationStore.list;
    let locations = this.props.locationStore.list;
    return (
      <Modal
        style={{    top: "10%",
        left: "10%",
        width: "75%",
        height: "75%",
        overflow: "auto",
        zIndex: 999
       }}
        open={true}
        onBackdropClick={this.handleFormClose}
        disableAutoFocus={true}
      >
        <Paper
          style={{
            width: "auto",
            height: "auto",
            padding: "20px",
            borderRadius: "15px"
          }}
        >
          <Typography style={{ width: "100%", margin: "10px" }} variant="h4">
            Team
          </Typography>
          <Typography style={{ width: "50px", margin: "10px" }} variant="body1">
            Name
          </Typography>
          <TextField
            variant="outlined"
            name="name"
            type="text"
            value={team.name}
            onChange={team.changeName}
            style={{ width: "30%", margin: "10px" }}
          />
          <Typography style={{ width: "50px", margin: "10px" }} variant="body1">
            Owner
          </Typography>
          <FormControl variant="outlined">
            <Select
              native
              value="owner"
              onChange={this.handleSelectChangeOwner}
              inputProps={{
                name: "owner",
                id: "outlined-owner-native-simple"
              }}
            >
              {team.owner ? (
                <option>{team.owner.person.firstName}</option>
              ) : (
                <option value=""></option>
              )}
              {owners &&
                owners.map(owner => <option>{owner.person.firstName}</option>)}
            </Select>
          </FormControl>
          <IconButton onClick={this.handleOpenCurrentOwnerForm}>
            <AddIcon />
          </IconButton>
          <Typography style={{ width: "50px", margin: "10px" }} variant="body1">
            Coach
          </Typography>
          <FormControl variant="outlined">
            <Select
              native
              value="coach"
              onChange={this.handleSelectChangeCoach}
              inputProps={{
                name: "coach",
                id: "outlined-coach-native-simple"
              }}
            >
              {team.coach ? (
                <option>{team.coach.person.firstName}</option>
              ) : (
                <option value=""></option>
              )}
              {coaches &&
                coaches.map(coach => <option>{coach.person.firstName}</option>)}
            </Select>
          </FormControl>
          <IconButton onClick={this.handleOpenCurrentCoachForm}>
            <AddIcon />
          </IconButton>

          <Typography style={{ width: "50px", margin: "10px" }} variant="body1">
            Association
          </Typography>
          <FormControl variant="outlined">
            <Select
              native
              value="association"
              onChange={this.handleSelectChangeAssociation}
              inputProps={{
                name: "association",
                id: "outlined-association-native-simple"
              }}
            >
              {team.association ? (
                <option>{team.association.name}</option>
              ) : (
                <option value=""></option>
              )}
              {associations &&
                associations.map(association => <option>{association.name}</option>)}
            </Select>
          </FormControl>
          <IconButton onClick={this.handleOpenCurrentAssociationForm}>
            <AddIcon />
          </IconButton>

          <Typography style={{ width: "50px", margin: "10px" }} variant="body1">
            Location
          </Typography>
          <FormControl variant="outlined">
            <Select
              native
              value="location"
              onChange={this.handleSelectChangeLocation}
              inputProps={{
                name: "location",
                id: "outlined-location-native-simple"
              }}
            >
              {team.location ? (
                <option>{team.location.name}</option>
              ) : (
                <option value=""></option>
              )}
              {locations &&
                locations.map(location => <option>{location.name}</option>)}
            </Select>
          </FormControl>
          <IconButton onClick={this.handleOpenCurrentLocationForm}>
            <AddIcon />
          </IconButton>

          {team.players && <PlayerTransfer team={team} />}
          <div style={{ width: "100%" }}>
            <Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
              cancel
            </Button>
            {targetTeam ? (
              <>
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.updateTargetTeam}
              >
                update
              </Button>
              <Button
              style={{ margin: "10px" }}
              color="primary"
              variant="contained"
              onClick={this.deleteTargetTeam}
            >
              Delete
            </Button>
            </>
            ) : (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.submitCurrentTeam}
              >
                Create
              </Button>
            )}
          </div>
        </Paper>
      </Modal>
    );
  }
}

export default TeamForm;
