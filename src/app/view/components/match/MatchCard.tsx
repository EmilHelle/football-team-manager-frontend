import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Divider } from "@material-ui/core";
import Match from "../../../model/Match";
import databaseAccessor from "../../../backend/data/DatabaseAccessor";

@inject("userStore", "matchStore", "rootView", "authStore")
@observer
class MatchCard extends React.Component<any, any> {
  
  constructor(props) {
    super(props);
    this.state = {
      detailsExpanded: false,
    };
  }

  handleChange = (event: React.ChangeEvent<{}>) => {
    this.setState({ detailsExpanded: !this.state.detailsExpanded });
  };

  editMatch = (match: Match) => {
    console.log("editMatch=>");
    this.props.matchStore.setTarget(match);
    this.props.rootView.matchView.setTargetFormOpen(true);
  }
  
  async isHomeWinner() {
    let { match } = this.props;
    let homeTeamId = match.homeTeam.id;
    let matchId = match.id;
    //let result = await databaseAccessor.getSingle("results", matchId, homeTeamId); 
    //Using hardcoded ids for testing
    let result = await databaseAccessor.getSingle("results", 2, this.props.authStore.loggedInUser.token, 2); 
    if (result) {
      if (result.matchResult ==="WIN") {
        return true;
      }
    } else {
      return false;
    }
  };

  async getHomeScore() {
    let { match } = this.props;
    let homeTeamId = match.homeTeam.id;
    let matchId = match.id;
    //Using hardcoded ids for testing
    // let result = await databaseAccessor.getSingle("results", matchId, homeTeamId);
    let result = await databaseAccessor.getSingle("results", 2, this.props.authStore.loggedInUser.token, 2); 
    if (result) {
      return result.score;
    } else {
      return "?";
    }
  };

  async getAwayScore() {
    let { match } = this.props;
    let awayTeamId = match.awayTeam.id;
    let matchId = match.id; 
    //Using hardcoded ids for testing
    //let result = await databaseAccessor.getSingle("results", matchId, awayTeamId);
    let result = await databaseAccessor.getSingle("results", 2, this.props.authStore.loggedInUser.token, 2); 
    if (result) {
      return result.score;
    } else {
      return "?";
    }
  };

  render() {
    let { match } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    let { detailsExpanded } = this.state;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <Card style={{ 
        color: theme.currentTheme.textSecondary,
        backgroundColor: theme.currentTheme.mainLight, 
        maxWidth: "70%",
        margin: "10px" }}>
        <CardContent>
          {loggedInUser.user.isAdmin && (
            <IconButton
              style={{ float: "right" }}
              onClick={() => this.editMatch(match)}
            >
              <EditIcon />
            </IconButton>
          )}
          {console.log(match)}
          {match.name && (
            <Typography 
            variant="h6" 
            gutterBottom 
            style={{color: theme.currentTheme.textMain}}>
              {match.name}
            </Typography>
          )}
          <Divider/>
          {match.date && (
            <Typography gutterBottom>
              Match played {new Date(match.date).getDate()}/
              {new Date(match.date).getMonth()}/
              {new Date(match.date).getFullYear()} {new Date(match.date).getHours()}:
              {(new Date(match.date).getMinutes() < 10 ? "0" : "") + new Date(match.date).getMinutes()}
            </Typography>
          )}
          {this.isHomeWinner !== null && 
            <Typography>
            <b>Winner:</b> {this.isHomeWinner ? match.homeTeam.name : match.awayTeam.name}!
            </Typography>
          }
          {loggedInUser.token && (
            <ExpansionPanel
              elevation={0}
              expanded={detailsExpanded}
              onChange={this.handleChange}
              style={{borderRadius: "5px", color: theme.currentTheme.textMain}}
            >
              <ExpansionPanelSummary
                style={{ backgroundColor: theme.currentTheme.mainLight, color: theme.currentTheme.textSecondary }}
                expandIcon={<ExpandMoreIcon />}
              >
                <Typography><i>Match details</i></Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails style={{ backgroundColor: theme.currentTheme.mainLight }}>
                <Typography>
                  <b>Location: </b>{match.location ? match.location.name : " -"}
                  <br/>
                  <b>Season: </b>{match.season ? match.season.name : " -"}
                  <br/>
                  <b>Score: </b>
                  {(match.homeTeam.name + " " + 3 + 
                  " - " + 2 + " " + match.awayTeam.name)}
                  {//hardcoded number should be this.getHomeScore, but it only
                 /* prints the promise*/}
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )}
        </CardContent>
      </Card>
    );
  }
}

export default MatchCard;
