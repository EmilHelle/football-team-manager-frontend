import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Match from "../../../../model/Match";
import Team from "../../../../model/Team";

@inject("rootView", "matchStore", "teamStore", "authStore")
@observer
class MatchForm extends React.Component<any, any> {
	handleFormClose = () => {
		let {
			rootView: {
				matchView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			matchStore: { target, current, setTarget, setCurrent }
		} = this.props;
		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(new Match(new Date(), new Team(""), new Team("")));
		} else {
			setCurrentFormOpen(false);
			setCurrent(new Match(new Date(), new Team(""), new Team("")));
		}
	};

	handleSelectChangeAway = e => {
		let targetValue = e.target.value;
		var match = this.props.teamStore.teams.find(team => team.name === targetValue);
		this.props.match.setAwayTeam(match);
	};

	handleSelectChangeHome = e => {
		let targetValue = e.target.value;
		var match = this.props.teamStore.teams.find(team => team.name === targetValue);
		this.props.match.setHomeTeam(match);
	};

	submitCurrentMatch = () => {
		databaseAccessor.create(
			"matches",
			this.props.matchStore.current.matchToJson(),
			this.props.authStore.loggedInUser.token
		);
		this.props.matchStore.add(this.props.matchStore.current);
		this.props.matchStore.scrollToBottom();
		//close modal
		this.props.rootView.matchView.setCurrentFormOpen(false);
	};

	updateTargetMatch = () => {
		databaseAccessor.update(
		  "matches",
		  this.props.matchStore.target.matchToJson(),
      this.props.matchStore.target.id,
      this.props.authStore.loggedInUser.token
    );
     //close modal
	   this.props.rootView.matchView.setTargetFormOpen(false);
	  };
	
	  deleteTargetMatch = () => {
      databaseAccessor.delete(
        "matches",
        this.props.matchStore.target.matchToJson(),
        this.props.matchStore.target.id,
        this.props.authStore.loggedInUser.token
      );
      this.props.matchStore.remove(this.props.matchStore.target);
      //close modal
      this.props.rootView.matchView.setTargetFormOpen(false);
	  };

	render() {
		let { match } = this.props;
		let targetMatch = this.props.matchStore.target;
		let teams = this.props.teamStore.list;
		return (
			<Modal
				style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "100%",
						height: "100%",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
						Match
					</Typography>
					<div style={{ display: "flex", flexDirection: "row" }}>
						<div style={{ margin: "10px" }}>
							<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
								Home team
							</Typography>
							<FormControl variant="outlined">
								<Select
									native
									value="team"
									onChange={this.handleSelectChangeHome}
									inputProps={{
										name: "home-team",
										id: "outlined-team-native-simple"
									}}
								>
									<option>{match.homeTeam.name}</option>
									{teams.map(team => (
										<option>{team.name}</option>
									))}
								</Select>
							</FormControl>
						</div>
						<div style={{ margin: "10px" }}>
							<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
								Away team
							</Typography>
							<FormControl variant="outlined">
								<Select
									native
									value="team"
									onChange={this.handleSelectChangeAway}
									inputProps={{
										name: "away-team",
										id: "outlined-team-native-simple"
									}}
								>
									<option>{match.awayTeam.name}</option>
									{teams.map(team => (
										<option>{team.name}</option>
									))}
								</Select>
							</FormControl>
						</div>
					</div>
					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							cancel
						</Button>
						{targetMatch ? (
							<>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.updateTargetMatch}
								>
									update
								</Button>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.deleteTargetMatch}
								>
									Delete
								</Button>
							</>
						) : (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.submitCurrentMatch}
							>
								Create
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default MatchForm;
