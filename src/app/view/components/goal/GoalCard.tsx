import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Goal from "../../../model/Goal";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "goalStore", "rootView")
@observer
class GoalCard extends React.Component<any, any> {
	editGoal = (goal: Goal) => {
		console.log("editGoal=>");
		this.props.goalStore.setTarget(goal);
		this.props.rootView.goalView.setTargetFormOpen(true);
	  };
  render() {
    let { goal } = this.props;
    let {
      userStore: { currentUser }
	} = this.props;
	let {
		authStore: { loggedInUser }
	} = this.props;
    let {
      rootView: { theme }
    } = this.props;

		return (
			<Card style={{ 
				color: theme.currentTheme.textSecondary,
				backgroundColor: theme.currentTheme.mainLight, 
				maxWidth: "60%",
				minWidth: "450px",
				margin: "10px" }}>
        		<CardContent>
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ float: "right" }} onClick={() => this.editGoal(goal)}>
							<EditIcon />
						</IconButton>
					)}
					{goal.typeOfGoal && (
						<Typography variant="h6" gutterBottom>
							<i>{goal.typeOfGoal}</i>
						</Typography>
					)}
				</CardContent>
			</Card>
		);
	}
}

export default GoalCard;
