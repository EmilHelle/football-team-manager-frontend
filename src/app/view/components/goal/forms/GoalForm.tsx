import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Goal from "../../../../model/Goal";

@inject("rootView", "goalStore", "authStore")
@observer
class GoalForm extends React.Component<any, any> {
  handleFormClose = () => {
    let {
      rootView: {
        goalView: { setCurrentFormOpen, setTargetFormOpen }
      }
    } = this.props;
    let {
      goalStore: { target, current, setTarget, setCurrent }
    } = this.props;
    if (target) {
      setTargetFormOpen(false);
      setTarget(null);
    }
    if (target && current) {
      setCurrentFormOpen(false);
      setCurrent(new Goal(""));
    } else {
      setCurrentFormOpen(false);
      setCurrent(new Goal(""));
    }
  };

  submitCurrentGoal = () => {
    databaseAccessor.create(
      "goaltypes",
      this.props.goalStore.current.goalToJson(),
      this.props.authStore.loggedInUser.token
    );
    this.props.goalStore.add(this.props.goalStore.current);
    this.props.goalStore.scrollToBottom();
    //close modal
    this.props.rootView.goalView.setCurrentFormOpen(false);
  };

  updateTargetGoal = () => {
    databaseAccessor.update(
      "goaltypes",
      this.props.goalStore.target.goalToJson(),
      this.props.goalStore.target.id,
      this.props.authStore.loggedInUser.token
	);
	 //close modal
	 this.props.rootView.goalView.setTargetFormOpen(false);
  };

  deleteTargetGoal = () => {
    databaseAccessor.delete(
      "goaltypes",
      this.props.goalStore.target.goalToJson(),
      this.props.goalStore.target.id,
      this.props.authStore.loggedInUser.token
    );
    this.props.goalStore.remove(this.props.goalStore.target);
	  //close modal
	  this.props.rootView.goalView.setTargetFormOpen(false);
  };

  render() {
    let { goal } = this.props;
    let {
      goalStore: { target }
    } = this.props;
    let targetGoal = target;
    return (
      <Modal
        style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
        open={true}
        onBackdropClick={this.handleFormClose}
        disableAutoFocus={true}
      >
        <Paper
          style={{
            width: "100%",
            height: "100%",
            padding: "20px",
            borderRadius: "15px"
          }}
        >
          <Typography style={{ width: "100%", margin: "10px" }} variant="h4">
            Goal
          </Typography>
          <TextField
            label="Type"
            variant="outlined"
            name="name"
            type="text"
            value={goal.typeOfGoal}
            onChange={goal.changeName}
            style={{ width: "50%", margin: "10px" }}
          />
          <div style={{ width: "100%" }}>
            <Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
              Cancel
            </Button>
            {targetGoal ? (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.updateTargetGoal}
              >
                Update
              </Button>
            ) : (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.submitCurrentGoal}
              >
                Create
              </Button>
            )}
          </div>

          <Button
            style={{ margin: "10px" }}
            color="primary"
            variant="contained"
            onClick={this.deleteTargetGoal}
          >
            Delete
          </Button>
        </Paper>
      </Modal>
    );
  }
}

export default GoalForm;
