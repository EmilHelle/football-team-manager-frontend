import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Player from "../../../model/Player";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "playerStore", "rootView")
@observer
class PlayerCard extends React.Component<any, any> {
	editPlayer = (player: Player) => {
		console.log("editPlayer=> player:", player);
		this.props.playerStore.setTarget(player);
		console.log("editPlayer=> targetPlayer:", this.props.playerStore.target);
		this.props.rootView.playerView.setTargetFormOpen(true);
	};
	render() {
		let { player } = this.props;
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<Card
				style={{
					backgroundColor: theme.currentTheme.mainLight,
					maxWidth: "60%",
					minWidth: "450px",
					margin: "10px"
				}}
			>
				<CardContent style={{ color: theme.currentTheme.textSecondary }}>
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ float: "right" }} onClick={() => this.editPlayer(player)}>
							<EditIcon />
						</IconButton>
					)}
					{player.person.firstName && (
						<Typography variant="h6" gutterBottom style={{ color: theme.currentTheme.textMain }}>
							{player.person.firstName} {""}
							{player.person.lastName}
						</Typography>
					)}

					<Divider />
					{player.person.dateOfBirth && (
						<Typography gutterBottom>
							<b>Date of birth: </b>
							{player.person.dateOfBirth}
						</Typography>
					)}
					{player.person.address && (
						<Typography gutterBottom>
							<b>Address:</b>
							<Typography variant="body2" style={{ margin: "5px 0 0 0" }} gutterBottom>
								{player.person.address.addressLine1
									? ["" + player.person.address.addressLine1]
									: ""}
								{player.person.address.addressLine2
									? [", " + player.person.address.addressLine2]
									: ""}
								{player.person.address.addressLine3
									? [", " + player.person.address.addressLine3]
									: ""}
							</Typography>
							{player.person.address.postalCode && (
								<Typography variant="body2" component="h2">
									{player.person.address.postalCode}
								</Typography>
							)}
							{player.person.address.city && (
								<Typography variant="body2" component="h2">
									{player.person.address.city}
								</Typography>
							)}
							{player.person.address.country && (
								<Typography variant="body2" component="h2">
									{player.person.address.country}
								</Typography>
							)}
						</Typography>
					)}
					{player.team.name && (
						<Typography gutterBottom>
							<b>Team: </b>
							{player.team.name}
						</Typography>
					)}
					{player.normalPosition && (
						<Typography gutterBottom>
							<b>Position: </b>
							{player.normalPosition}
						</Typography>
					)}
					{player.number && (
						<Typography gutterBottom>
							<b>Number: </b>
							{player.number}
						</Typography>
					)}
				</CardContent>
			</Card>
		);
	}
}

export default PlayerCard;
