import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Player from "../../../../model/Player";
import Person from "../../../../model/Person";
import Team from "../../../../model/Team";
import Owner from "../../../../model/Owner";
import Coach from "../../../../model/Coach";
import Association from "../../../../model/Association";
import Location from "../../../../model/Location";
import Address from "../../../../model/Address";

@inject("rootView", "playerStore", "teamStore", "addressStore", "authStore", "personStore")
@observer
class PlayerForm extends React.Component<any, any> {
	componentDidMount() {
		this.props.personStore.setList([]);
		this.getPeople();
		this.props.teamStore.setList([]);
		this.getTeams();
	}
	async getPeople() {
		var returnedPersons = await databaseAccessor.getAll(
			"people",
			this.props.authStore.loggedInUser.token
		);
		for (let index = 0; index < returnedPersons.length; index++) {
			const person = returnedPersons[index];
			this.props.personStore.add(
				new Person(
					person.firstName,
					person.lastName,
					person.dateOfBirth,
					new Address(
						person.address.postalCode,
						person.address.city,
						person.address.country,
						person.address.addressLine1,
						person.address.addressLine2,
						person.address.addressLine3,
						person.address.id
					),
					person.id
				)
			);
		}
	}

	async getTeams() {
		var returnedTeams = await databaseAccessor.getAll(
			"teams",
			this.props.authStore.loggedInUser.token
		);
		for (let index = 0; index < returnedTeams.length; index++) {
			const team = returnedTeams[index];
			this.props.teamStore.add(
				new Team(
					team.name,
					new Owner(
						new Person(
							team.owner.person.firstName,
							team.owner.person.lastName,
							new Date(team.owner.person.dateOfBirth),
							new Address(
								team.owner.person.address.postalCode,
								team.owner.person.address.city,
								team.owner.person.address.country,
								team.owner.person.address.addressLine1,
								team.owner.person.address.addressLine2,
								team.owner.person.address.addressLine3,
								team.owner.person.address.id
							),
							team.owner.person.id
						),
						team.owner.id
					),
					new Coach(
						new Person(
							team.coach.person.firstName,
							team.coach.person.lastName,
							new Date(team.coach.person.dateOfBirth),
							new Address(
								team.coach.person.address.postalCode,
								team.coach.person.address.city,
								team.coach.person.address.country,
								team.coach.person.address.addressLine1,
								team.coach.person.address.addressLine2,
								team.coach.person.address.addressLine3,
								team.coach.person.address.id
							),
							team.coach.person.id
						),
						team.coach.id
					),
					new Association(team.association.name, team.association.description, team.association.id),
					new Location(
						new Address(
							team.location.address.postalCode,
							team.location.address.city,
							team.location.address.country,
							team.location.address.addressLine1,
							team.location.address.addressLine2,
							team.location.address.addressLine3,
							team.location.address.id
						),
						team.location.name,
						team.location.description,
						team.location.id
					),
					null,
					team.id
				)
			);
		}
	}

	handleFormClose = () => {
		let {
			rootView: {
				playerView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			playerStore: { target, current, setTarget, setCurrent }
		} = this.props;

		let targetPlayer = target;
		let currentPlayer = current;
		let setTargetPlayer = setTarget;

		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(new Player(new Person(""), new Team(""), "", null, null));
		} else {
			setCurrentFormOpen(false);
			setCurrent(new Player(new Person(""), new Team(""), "", null, null));
		}
	};

	handleSelectChange = e => {
		let targetValue = e.target.value;
		var match = this.props.teamStore.list.find(team => team.name === targetValue);
		this.props.player.setTeam(match);
	};

	handleSelectChangePerson = e => {
		let targetValue = e.target.value;
		var person = this.props.personStore.list.find(person => person.firstName === targetValue);
		this.props.player.setPerson(person);
	};

	submitCurrentPlayer = () => {
		databaseAccessor.create(
			"players",
			this.props.playerStore.current.playerCreateToJson(),
			this.props.authStore.loggedInUser.token
		);
		this.props.playerStore.add(this.props.playerStore.current);
		this.props.playerStore.scrollToBottom();
		//close modal
		this.props.rootView.playerView.setCurrentFormOpen(false);
	};

	updateTargetPlayer = () => {
		databaseAccessor.update(
			"players",
			this.props.playerStore.target.playerToJson(),
			this.props.playerStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		 //close modal
		 this.props.rootView.playerView.setTargetFormOpen(false);
	};

	deleteTargetPlayer = () => {
		databaseAccessor.delete(
			"players",
			this.props.playerStore.target.playerToJson(),
			this.props.playerStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		this.props.playerStore.remove(this.props.playerStore.target);
		//close modal
		this.props.rootView.playerView.setTargetFormOpen(false);
	};

	render() {
		let { player } = this.props;
		let targetPlayer = this.props.playerStore.target;
		let teams = this.props.teamStore.list;
		let persons = this.props.personStore.list;
		return (
			<Modal
				style={{
					top: "10%",
					left: "10%",
					width: "75%",
					height: "75%",
					zIndex: 999
				}}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<div>
					<Paper
						style={{
							width: "100%",
							height: "100%",
							padding: "20px",
							borderRadius: "15px"
						}}
					>
						<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
							Player
						</Typography>

						<div style={{ margin: "10px" }}>
							{targetPlayer && (
								<Typography style={{ width: "100%", margin: "10px" }} variant="body1">
									Edit player
								</Typography>
							)}
							{!targetPlayer && (
								<div>
									<Typography style={{ width: "100%", margin: "10px" }} variant="body1">
										Create a player
									</Typography>
									<FormControl variant="outlined">
										<Select
											style={{ width: "100%" }}
											native
											value="person"
											onChange={this.handleSelectChangePerson}
											inputProps={{
												name: "person",
												id: "outlined-person-native-simple"
											}}
										>
											{player.person.firstName ? (
												<option>{player.person.firstName}</option>
											) : (
												<option value=""></option>
											)}
											{persons.map(person => (
												<option>{person.firstName}</option>
											))}
										</Select>
									</FormControl>
								</div>
							)}
						</div>

						<TextField
							label="Position"
							variant="outlined"
							name="name"
							type="text"
							value={player.normalPosition}
							onChange={player.changeNormalPosition}
							style={{ width: "45%", margin: "10px" }}
						/>

						<TextField
							label="Number"
							variant="outlined"
							name="name"
							type="text"
							value={player.number}
							onChange={player.changeNumber}
							style={{ width: "45%", margin: "10px" }}
						/>
						<div style={{ display: "flex", flexDirection: "row" }}>
							<div style={{ margin: "10px" }}>
								<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
									Team
								</Typography>
								<FormControl variant="outlined">
									<Select
										style={{ width: "100%" }}
										native
										value="team"
										onChange={this.handleSelectChange}
										inputProps={{
											name: "team",
											id: "outlined-team-native-simple"
										}}
									>
										{player.team ? <option>{player.team.name}</option> : <option value=""></option>}
										{teams.map(team => (
											<option>{team.name}</option>
										))}
									</Select>
								</FormControl>
							</div>
						</div>
						<div style={{ width: "75%" }}>
							<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
								cancel
							</Button>
							{targetPlayer ? (
								<>
									<Button
										style={{ margin: "10px" }}
										color="primary"
										variant="contained"
										onClick={this.updateTargetPlayer}
									>
										Update
									</Button>
									<Button
										style={{ margin: "10px" }}
										color="primary"
										variant="contained"
										onClick={this.deleteTargetPlayer}
									>
										Delete
									</Button>
								</>
							) : (
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.submitCurrentPlayer}
								>
									Create
								</Button>
							)}
						</div>
					</Paper>
				</div>
			</Modal>
		);
	}
}

export default PlayerForm;
