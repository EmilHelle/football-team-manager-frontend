import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { inject, observer } from "mobx-react";

@inject("rootView")
@observer
class Loading extends React.Component<any, any> {
  render() {
    let {
      rootView: { theme }
    } = this.props;
    return (
      <CircularProgress
        style={{ color: theme.currentTheme.main, margin: "20%" }}
        size={100}
      />
    );
  }
}

export default Loading;
