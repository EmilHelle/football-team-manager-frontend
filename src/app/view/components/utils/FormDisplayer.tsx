import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import CoachForm from "../coach/forms/CoachForm";
import OwnerForm from "../owner/forms/OwnerForm";
import MatchForm from "../match/forms/MatchForm";
import TeamForm from "../team/forms/TeamForm";
import PlayerForm from "../player/forms/PlayerForm";
import PersonForm from "../person/forms/PersonForm";
import SeasonForm from "../season/forms/SeasonForm";
import LocationForm from "../location/forms/LocationForm";
import GoalForm from "../goal/forms/GoalForm";
import AssociationForm from "../association/forms/AssociationForm";
import AddressForm from "../address/forms/AddressForm";

@inject(
  "rootView",
  "coachStore",
  "ownerStore",
  "teamStore",
  "matchStore",
  "playerStore",
  "personStore",
  "locationStore",
  "seasonStore",
  "goalStore",
  "associationStore",
  "addressStore"
)
@observer
class FormDisplayer extends Component<any, any> {
  render() {
    let currentCoachFormOpen = this.props.rootView.coachView.currentFormOpen;
    let targetCoachFormOpen = this.props.rootView.coachView.targetFormOpen;
    let currentCoach = this.props.coachStore.current;
    let targetCoach = this.props.coachStore.target;

    let currentOwnerFormOpen = this.props.rootView.ownerView.currentFormOpen;
    let targetOwnerFormOpen = this.props.rootView.ownerView.targetFormOpen;
    let currentOwner = this.props.ownerStore.current;
    let targetOwner = this.props.ownerStore.target;

    let currentMatchFormOpen = this.props.rootView.matchView.currentFormOpen;
    let targetMatchFormOpen = this.props.rootView.matchView.targetFormOpen;
    let currentMatch = this.props.matchStore.current;
    let targetMatch = this.props.matchStore.target;

    let currentTeamFormOpen = this.props.rootView.teamView.currentFormOpen;
    let targetTeamFormOpen = this.props.rootView.teamView.targetFormOpen;
    let currentTeam = this.props.teamStore.current;
    let targetTeam = this.props.teamStore.target;

    let currentPlayerFormOpen = this.props.rootView.playerView.currentFormOpen;
    let targetPlayerFormOpen = this.props.rootView.playerView.targetFormOpen;
    let currentPlayer = this.props.playerStore.current;
    let targetPlayer = this.props.playerStore.target;

    let currentPersonFormOpen = this.props.rootView.personView.currentFormOpen;
    let targetPersonFormOpen = this.props.rootView.personView.targetFormOpen;
    let currentPerson = this.props.personStore.current;
    let targetPerson = this.props.personStore.target;

    let currentSeasonFormOpen = this.props.rootView.seasonView.currentFormOpen;
    let targetSeasonFormOpen = this.props.rootView.seasonView.targetFormOpen;
    let currentSeason = this.props.seasonStore.current;
    let targetSeason = this.props.seasonStore.target;

    let currentLocationFormOpen = this.props.rootView.locationView
      .currentFormOpen;
    let targetLocationFormOpen = this.props.rootView.locationView
      .targetFormOpen;
    let currentLocation = this.props.locationStore.current;
    let targetLocation = this.props.locationStore.target;

    let currentGoalFormOpen = this.props.rootView.goalView.currentFormOpen;
    let targetGoalFormOpen = this.props.rootView.goalView.targetFormOpen;
    let currentGoal = this.props.goalStore.current;
    let targetGoal = this.props.goalStore.target;

    let currentAssociationFormOpen = this.props.rootView.associationView
      .currentFormOpen;
    let targetAssociationFormOpen = this.props.rootView.associationView
      .targetFormOpen;
    let currentAssociation = this.props.associationStore.current;
    let targetAssociation = this.props.associationStore.target;

    let currentAddressFormOpen = this.props.rootView.addressView.currentFormOpen;
    let targetAddressFormOpen = this.props.rootView.addressView.targetFormOpen;
    let currentAddress = this.props.addressStore.current;
    let targetAddress = this.props.addressStore.target;

    return (
      <div>
        {currentCoachFormOpen && <CoachForm coach={currentCoach} />}
        {targetCoachFormOpen && <CoachForm coach={targetCoach} />}

        {currentOwnerFormOpen && <OwnerForm owner={currentOwner} />}
        {targetOwnerFormOpen && <OwnerForm owner={targetOwner} />}

        {currentMatchFormOpen && <MatchForm match={currentMatch} />}
        {targetMatchFormOpen && <MatchForm match={targetMatch} />}

        {currentTeamFormOpen && <TeamForm team={currentTeam} />}
        {targetTeamFormOpen && <TeamForm team={targetTeam} />}

        {currentPlayerFormOpen && <PlayerForm player={currentPlayer} />}
        {targetPlayerFormOpen && <PlayerForm player={targetPlayer} />}

        {currentPersonFormOpen && <PersonForm person={currentPerson} />}
        {targetPersonFormOpen && <PersonForm person={targetPerson} />}

        {currentSeasonFormOpen && <SeasonForm season={currentSeason} />}
        {targetSeasonFormOpen && <SeasonForm season={targetSeason} />}

        {currentLocationFormOpen && <LocationForm location={currentLocation} />}
        {targetLocationFormOpen && <LocationForm location={targetLocation} />}

        {currentGoalFormOpen && <GoalForm goal={currentGoal} />}
        {targetGoalFormOpen && <GoalForm goal={targetGoal} />}

        {currentAssociationFormOpen && (
          <AssociationForm association={currentAssociation} />
        )}
        {targetAssociationFormOpen && (
          <AssociationForm association={targetAssociation} />
        )}

{currentAddressFormOpen && <AddressForm address={currentAddress} />}
        {targetAddressFormOpen && <AddressForm address={targetAddress} />}
      </div>
    );
  }
}

export default FormDisplayer;
