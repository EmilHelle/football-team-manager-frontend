import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Person from "../../../model/Person";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "personStore", "rootView")
@observer
class PersonCard extends React.Component<any, any> {
  editPerson = (person: Person) => {
    console.log("editPerson=>");
    this.props.personStore.setTarget(person);
    this.props.rootView.personView.setTargetFormOpen(true);
  };
  render() {
    let { person } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <Card style={{ 
        color: theme.currentTheme.textSecondary,
        backgroundColor: theme.currentTheme.mainLight, 
        maxWidth: "60%",
        minWidth: "450px",
        margin: "10px" }}>
        <CardContent>
          {loggedInUser.user.isAdmin && (
            <IconButton
              style={{ float: "right" }}
              onClick={() => this.editPerson(person)}
            >
              <EditIcon />
            </IconButton>
          )}

          <Typography
            variant="h6"
            component="h2"
            style={{ color: theme.currentTheme.textMain }}
          >
            <b>
              {person.lastName ? person.lastName : ""}
              {person.firstName ? [", " + person.firstName] : ""}
            </b>
          </Typography>
          <Divider />
          {person.dateOfBirth && (
            <Typography variant="body1" component="h2">
              <i>
                Born: {new Date(person.dateOfBirth).getDate()}/
                {new Date(person.dateOfBirth).getMonth()}/
                {new Date(person.dateOfBirth).getFullYear()}
              </i>
            </Typography>
          )}
          {person.address && (
            <div>
              <Typography
                variant="body2"
                style={{ margin: "5px 0 0 0" }}
                gutterBottom
              >
                <b>Address:</b>
                <br />
                {person.address.addressLine1
                  ? ["" + person.address.addressLine1]
                  : ""}
                {person.address.addressLine2
                  ? [", " + person.address.addressLine2]
                  : ""}
                {person.address.addressLine3
                  ? [", " + person.address.addressLine3]
                  : ""}
              </Typography>
              {person.address.postalCode && (
                <Typography variant="body2" component="h2">
                  {person.address.postalCode}
                </Typography>
              )}
              {person.address.city && (
                <Typography variant="body2" component="h2">
                  {person.address.city}
                </Typography>
              )}
              {person.address.country && (
                <Typography variant="body2" component="h2">
                  {person.address.country}
                </Typography>
              )}
            </div>
          )}
        </CardContent>
      </Card>
    );
  }
}

export default PersonCard;
