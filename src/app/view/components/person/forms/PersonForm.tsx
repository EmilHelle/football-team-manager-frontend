import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import { IconButton, Select, FormControl } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Address from "../../../../model/Address";
import Person from "../../../../model/Person";

@inject("rootView", "personStore", "addressStore", "authStore")
@observer
class PersonForm extends React.Component<any, any> {
	componentDidMount() {
		this.props.addressStore.setList([]);
		this.getAddresses();
	}

	async getAddresses() {
		var returnedAddresses = await databaseAccessor.getAll("addresses", this.props.authStore.loggedInUser.token);
		for (let index = 0; index < returnedAddresses.length; index++) {
			const address = returnedAddresses[index];
			this.props.addressStore.add(
				new Address(
					address.postalCode,
					address.city,
					address.country,
					address.addressLine1,
					address.addressLine2,
					address.addressLine3,
					address.id
				)
			);
		}
	}

	handleFormClose = () => {
		let {
			rootView: {
				personView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			personStore: { target, current, setTarget, setCurrent }
		} = this.props;
		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(new Person(""));
		} else {
			setCurrentFormOpen(false);
			setCurrent(new Person(""));
		}
	};

	handleOpenCurrentAddressForm = () => {
		this.props.rootView.addressView.setCurrentFormOpen(true);
	};

	handleSelectChangeAddress = e => {
		let targetValue = e.target.value;
		var address = this.props.addressStore.list.find(
			address => address.addressLine1 === targetValue
		);
		this.props.person.setAddress(address);
	};

  submitCurrentPerson = () => {
    databaseAccessor.create(
      "people",
      this.props.personStore.current.personToJson(),
      this.props.authStore.loggedInUser.token
	);
	this.props.personStore.add(this.props.personStore.current);
	this.props.personStore.scrollToBottom();
    //close modal
    this.props.rootView.personView.setCurrentFormOpen(false);
  };

  updateTargetPerson = () => {
    databaseAccessor.update(
      "people",
      this.props.personStore.target.personToJson(),
      this.props.personStore.target.id,
      this.props.authStore.loggedInUser.token
	);
	 //close modal
	 this.props.rootView.personView.setTargetFormOpen(false);
  };

  deleteTargetPerson = () => {
    databaseAccessor.delete(
      "people",
      this.props.personStore.target.personToJson(),
      this.props.personStore.target.id,
      this.props.authStore.loggedInUser.token
	);
	this.props.personStore.remove(this.props.personStore.target);
	//close modal
	this.props.rootView.personView.setTargetFormOpen(false);
  };

	render() {
		let { person } = this.props;
		let targetPerson = this.props.personStore.target;
		let addresses = this.props.addressStore.list;

		return (
			<Modal
				style={{
					top: "10%",
					left: "10%",
					width: "75%",
					height: "75%",
					overflow: "auto"
				}}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "auto",
						height: "auto",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
						Person
					</Typography>
					<TextField
						label="First Name"
						variant="outlined"
						name="name"
						type="text"
						value={person.firstName}
						onChange={person.changeFirstName}
						style={{ width: "50%", margin: "10px", inlineSize: "auto" }}
					/>
					<TextField
						label="Last Name"
						variant="outlined"
						name="name"
						type="text"
						value={person.lastName}
						onChange={person.changeLastName}
						style={{ width: "50%", margin: "10px", inlineSize: "auto" }}
					/>

					<Typography variant="body2" style={{ width: "100%", margin: "10px" }}>
						Date of birth
					</Typography>
					<TextField
						variant="outlined"
						//defaultValue={person.dateOfBirth.toString()}
						name="name"
						type="date"
						onChange={person.changeDateOfBirth}
						style={{ width: "50%", margin: "10px", inlineSize: "auto" }}
					/>

					<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
						Address
					</Typography>
					<FormControl variant="outlined">
						<Select
							native
							value="address"
							onChange={this.handleSelectChangeAddress}
							inputProps={{
								name: "address",
								id: "outlined-person-native-simple"
							}}
						>
							{person.address ? (
								<option>{person.address.addressLine1}</option>
							) : (
								<option value=""></option>
							)}
							{addresses && addresses.map(address => <option>{address.addressLine1}</option>)}
						</Select>
					</FormControl>
					<IconButton onClick={this.handleOpenCurrentAddressForm}>
						<AddIcon />
					</IconButton>

					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							cancel
						</Button>
						{targetPerson && (
							<div>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.updateTargetPerson}
								>
									Update
								</Button>

								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.deleteTargetPerson}
								>
									Delete
								</Button>
							</div>
						)}
						{!targetPerson && (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.submitCurrentPerson}
							>
								Create
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default PersonForm;
