import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import { Address } from "cluster";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "addressStore", "rootView")
@observer
class AddressCard extends React.Component<any, any> {
  editAddress = (address: Address) => {
    console.log("editAddress=>");
    this.props.addressStore.setTarget(address);
    this.props.rootView.addressView.setTargetFormOpen(true);
  };

  render() {
    let { address } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <Card style={{ 
        backgroundColor: theme.currentTheme.mainLight, 
        color: theme.currentTheme.textSecondary, 
        maxWidth: "60%",
        minWidth: "450px", 
        margin: "10px" }}>
        <CardContent>
          {loggedInUser.user.isAdmin && (
            <IconButton
              style={{ float: "right" }}
              onClick={() => this.editAddress(address)}
            >
              <EditIcon />
            </IconButton>
          )}
          <Typography style={{color: theme.currentTheme.textMain}} variant="h6" gutterBottom>
              {address.addressLine1 ? [" " + address.addressLine1] : ""}
              {address.addressLine2 ? [", " + address.addressLine2] : ""}
              {address.addressLine3 ? [", " + address.addressLine3] : ""}
          </Typography>
          <Divider/>
          <Typography variant="body2" component="h2">
            {address.postalCode ? <>{address.postalCode}</> : <>..</>}
          </Typography>
          <Typography variant="body2" component="h2">
            {address.city ? <>{address.city}</> : <>..</>}
          </Typography>
          <Typography variant="body2" component="h2">
            {address.country ? <>{address.country}</> : <>..</>}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

export default AddressCard;
