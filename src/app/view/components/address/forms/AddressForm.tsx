import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Address from "../../../../model/Address";

@inject("rootView", "addressStore", "authStore")
@observer
class AddressForm extends React.Component<any, any> {
  handleFormClose = () => {
    let {
      rootView: {
        addressView: { setCurrentFormOpen, setTargetFormOpen }
      }
    } = this.props;
    let {
      addressStore: { target, current, setTarget, setCurrent }
    } = this.props;
    let targetAddress = target;
    let currentAddress = current;
    let setTargetAddress = setTarget;
    if (targetAddress) {
      setTargetFormOpen(false);
      setTargetAddress(null);
    }
    if (targetAddress && currentAddress) {
      setCurrentFormOpen(false);
      setCurrent(new Address(0, "", "", ""));
    } else {
      setCurrentFormOpen(false);
      setCurrent(new Address(0, "", "", ""));
    }
  };

  submitCurrentAddress = () => {
    databaseAccessor.create(
      "addresses",
      this.props.addressStore.current.addressToJson(),
      this.props.authStore.loggedInUser.token
    );
	this.props.addressStore.add(this.props.addressStore.current);
	this.props.addressStore.scrollToBottom();
    //close modal
    this.props.rootView.addressView.setCurrentFormOpen(false);
  };

  updateTargetAddress = () => {
    databaseAccessor.update(
      "addresses",
      this.props.addressStore.target.addressToJson(),
      this.props.addressStore.target.id,
      this.props.authStore.loggedInUser.token
    );
    //close modal
    this.props.rootView.addressView.setTargetFormOpen(false);
  };

  deleteTargetAddress= () => {
    databaseAccessor.delete(
      "addresses",
      this.props.addressStore.target.addressToJson(),
      this.props.addressStore.target.id,
      this.props.authStore.loggedInUser.token );
	  this.props.addressStore.remove(this.props.addressStore.target);
    //close modal
    this.props.rootView.addressView.setTargetFormOpen(false);
  };

  render() {
    let { address } = this.props;
    let {
      addressStore: { target }
    } = this.props;
    let targetAddress = target;

    return (
      <Modal
        style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
        open={true}
        onBackdropClick={this.handleFormClose}
        disableAutoFocus={true}
      >
        <Paper
          style={{
            width: "100%",
            height: "100%",
            padding: "20px",
            borderRadius: "15px"
          }}
        >
          <Typography style={{ width: "100%", margin: "10px" }} variant="h4">
            Address
          </Typography>

          <div>
            <TextField
              required
              label="Address 1"
              variant="outlined"
              name="addressLine1"
              type="text"
              value={address.addressLine1}
              onChange={address.changeAddressLine1}
              style={{ width: "45%", margin: "10px" }}
            />

            <TextField
              required
              label="Postal code"
              variant="outlined"
              name="postalCode"
              type="number"
              value={address.postalCode}
              onChange={address.changePostalCode}
              style={{ width: "45%", margin: "10px" }}
            />

            <TextField
              label="Address 2"
              variant="outlined"
              name="addressLine2"
              type="text"
              value={address.addressLine2}
              onChange={address.changeAddressLine2}
              style={{ width: "45%", margin: "10px" }}
            />

            <TextField
              required
              label="City"
              variant="outlined"
              name="city"
              type="text"
              value={address.city}
              onChange={address.changeCity}
              style={{ width: "45%", margin: "10px" }}
            />

            <TextField
              label="Address 3"
              variant="outlined"
              name="addressLine3"
              type="text"
              value={address.addressLine3}
              onChange={address.changeAddressLine3}
              style={{ width: "45%", margin: "10px" }}
            />

            <TextField
              required
              label="Country"
              variant="outlined"
              name="country"
              type="text"
              value={address.country}
              onChange={address.changeCountry}
              style={{ width: "45%", margin: "10px" }}
            />
          </div>
          <div style={{ width: "100%" }}>
            <Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
              Cancel
            </Button>
            {targetAddress && (
              <div>
                <Button
                  style={{ margin: "10px" }}
                  color="primary"
                  variant="contained"
                  onClick={this.updateTargetAddress}
                >
                  Update
                </Button>
                <Button
                  style={{ margin: "10px" }}
                  color="primary"
                  variant="contained"
                  onClick={this.deleteTargetAddress}
                >
                  Delete
                </Button>
              </div>
            )}
            {!targetAddress && (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.submitCurrentAddress}
              >
                Create
              </Button>
            )}
          </div>
        </Paper>
      </Modal>
    );
  }
}

export default AddressForm;
