import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Season from "../../../model/Season";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "seasonStore", "rootView")
@observer
class SeasonCard extends React.Component<any, any> {
	editSeason = (season: Season) => {
		console.log("editSeason=>");
		this.props.seasonStore.setTarget(season);
		this.props.rootView.seasonView.setTargetFormOpen(true);
	  };
	render() {
		let { season } = this.props;
		let {
			userStore: { currentUser } } = this.props;
			let {
				authStore: { loggedInUser }
			} = this.props;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<Card style={{ 
				color: theme.currentTheme.textSecondary,
				backgroundColor: theme.currentTheme.mainLight, 
				maxWidth: "60%",
				minWidth: "450px",
				margin: "10px" }}>
        		<CardContent>
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ float: "right" }} onClick={() => this.editSeason(season)}>
							<EditIcon />
						</IconButton>
					)}
					<Typography 
					variant="h6" 
					gutterBottom
					style={{color: theme.currentTheme.textMain}}>
						{season.name}
					</Typography>
					<Divider/>
					<Typography variant="body2" component="h2" gutterBottom>
						<b>Description:</b> <i>{season.description}</i>
					</Typography>
					<Typography variant="body2" component="h2" gutterBottom>
						<b>Start date: </b>
						<i>{new Date(season.startDate).getDate()}/
						{new Date(season.startDate).getMonth()}/
						{new Date(season.startDate).getFullYear()}</i>
					</Typography>
					<Typography variant="body2" component="h2" gutterBottom>
						<b>End date: </b> 
						<i>{new Date(season.endDate).getDate()}/
						{new Date(season.endDate).getMonth()}/
						{new Date(season.endDate).getFullYear()}</i>
					</Typography>
				</CardContent>
			</Card>
		);
	}
}

export default SeasonCard;
