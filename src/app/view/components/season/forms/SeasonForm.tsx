import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Season from "../../../../model/Season";

@inject("rootView", "seasonStore", "authStore")
@observer
class SeasonForm extends React.Component<any, any> {
	handleFormClose = () => {
		let {
			rootView: {
				seasonView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let targetSeason = this.props.seasonStore.target;
		let currentSeason = this.props.seasonStore.current;
		let setTargetSeason = this.props.seasonStore.setTarget;
		let setCurrentSeason = this.props.seasonStore.setCurrent;
		if (targetSeason) {
			setTargetFormOpen(false);
			setTargetSeason(null);
		}
		if (targetSeason && currentSeason) {
			setCurrentFormOpen(false);
			setCurrentSeason(new Season(new Date(), new Date(), ""));
		} else {
			setCurrentFormOpen(false);
			setCurrentSeason(new Season(new Date(), new Date(), ""));
		}
	};

	submitCurrentSeason = () => {
		databaseAccessor.create(
			"seasons",
			this.props.seasonStore.current.seasonToJson(),
			this.props.authStore.loggedInUser.token
		);
		this.props.seasonStore.add(this.props.seasonStore.current);
		this.props.seasonStore.scrollToBottom();
		//close modal
		this.props.rootView.seasonView.setCurrentFormOpen(false);
	};

	updateTargetSeason = () => {
		databaseAccessor.update(
			"seasons",
			this.props.seasonStore.target.seasonToJson(),
			this.props.seasonStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		 //close modal
		 this.props.rootView.seasonView.setTargetFormOpen(false);
	};

	deleteTargetSeason = () => {
		databaseAccessor.delete(
			"seasons",
			this.props.seasonStore.target.seasonToJson(),
			this.props.seasonStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		this.props.seasonStore.remove(this.props.seasonStore.target);
		//close modal
		this.props.rootView.seasonView.setTargetFormOpen(false);
	};

	render() {
		let { season } = this.props;
		let targetSeason = this.props.seasonStore.target;
		return (
			<Modal
				style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "100%",
						height: "100%",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
						Season
					</Typography>

					<TextField
						label="Start date"
						variant="outlined"
						name="name"
						type="date"
						value={season.startDate}
						onChange={season.changeStartDate}
						style={{ width: "50%", margin: "10px" }}
					/>

					<TextField
						label="End date"
						variant="outlined"
						name="name"
						type="date"
						value={season.endDate}
						onChange={season.changeEndDate}
						style={{ width: "50%", margin: "10px" }}
					/>

					<TextField
						label="Name"
						variant="outlined"
						name="name"
						type="text"
						value={season.name}
						onChange={season.changeName}
						style={{ width: "50%", margin: "10px" }}
					/>
					<TextField
						multiline
						rowsMax="6"
						label="Description"
						variant="outlined"
						name="name"
						type="text"
						value={season.description}
						onChange={season.changeDescription}
						style={{ width: "50%", margin: "10px" }}
					/>
					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							Cancel
						</Button>
						{targetSeason ? (
							<>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.updateTargetSeason}
								>
									Update
								</Button>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.deleteTargetSeason}
								>
									Delete
								</Button>
							</>
						) : (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.submitCurrentSeason}
							>
								Create
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default SeasonForm;
