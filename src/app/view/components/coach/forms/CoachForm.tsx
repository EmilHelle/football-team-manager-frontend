import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import AddIcon from "@material-ui/icons/Add";
import { FormControl, Select, IconButton } from "@material-ui/core";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Address from "../../../../model/Address";
import Person from "../../../../model/Person";
import Coach from "../../../../model/Coach";

@inject("rootView", "coachStore", "personStore", "authStore")
@observer
class CoachForm extends React.Component<any, any> {
	componentDidMount() {
		this.props.personStore.setList([]);
		this.getPeople();
	}


  async getPeople() {
    var returnedPersons = await databaseAccessor.getAll("people", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedPersons.length; index++) {
      	const person = returnedPersons[index];
		this.props.personStore.add(
			new Person(
				person.firstName,
				person.lastName,
				new Date(person.dateOfBirth),
				new Address(
					person.address.postalCode,
					person.address.city,
					person.address.country,
					person.address.addressLine1,
					person.address.addressLine2,
					person.address.addressLine3
				)
			)
		);
		}
	}

	handleFormClose = () => {
		let {
			rootView: {
				coachView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			coachStore: { target, current, setTarget, setCurrent }
		} = this.props;
		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(new Coach(new Person("")));
		} else {
			setCurrentFormOpen(false);
			setCurrent(new Coach(new Person("")));
		}
	};

	handleOpenCurrentPersonForm = () => {
		this.props.rootView.personView.setCurrentFormOpen(true);
	};

	handleSelectChangePerson = e => {
		let targetValue = e.target.value;
		var person = this.props.personStore.list.find(person => person.firstName === targetValue);
		this.props.coach.setPerson(person);
	};

  submitCurrentCoach = () => {
    databaseAccessor.create(
      "coaches",
      this.props.coachStore.current.coachToJson(), 
      this.props.authStore.loggedInUser.token
	);
	this.props.coachStore.add(this.props.coachStore.current);
	this.props.coachStore.scrollToBottom();
	//close modal
	this.props.rootView.coachView.setCurrentFormOpen(false);
  };

  updateTargetCoach = () => {
    databaseAccessor.update(
      "coaches",
      this.props.coachStore.target.coachToJson(),
      this.props.coachStore.target.id, 
      this.props.authStore.loggedInUser.token
	);
	 //close modal
	 this.props.rootView.coachView.setTargetFormOpen(false);
  };

  deleteTargetCoach = () => {
    databaseAccessor.delete(
      "coaches",
      this.props.coachStore.target.coachToJson(),
      this.props.coachStore.target.id,
      this.props.authStore.loggedInUser.token
	);
	this.props.coachStore.remove(this.props.coachStore.target);
	   //close modal
	   this.props.rootView.coachView.setTargetFormOpen(false);
  };

	render() {
		let { coach } = this.props;
		let targetCoach = this.props.coachStore.target;
		let persons = this.props.personStore.list;
		return (
			<Modal
				style={{
					top: "10%",
					left: "10%",
					width: "75%",
					height: "75%",
					overflow: "auto"
				}}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "auto",
						height: "auto",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
						Coach
					</Typography>
					{targetCoach && (
						<Typography style={{ margin: "10px" }} variant="body1">
							A coach can only be deleted
						</Typography>
					)}

					{!targetCoach && (
						<Typography>
							<Typography style={{ margin: "10px" }} variant="body1">
								Create a new coach
							</Typography>
							<div>
								<FormControl variant="outlined">
									<Select
										native
										value="person"
										onChange={this.handleSelectChangePerson}
										inputProps={{
											name: "person",
											id: "outlined-address-native-simple"
										}}
									>
										{coach.person ? (
											<option>{coach.person.firstName}</option>
										) : (
											<option value=""></option>
										)}
										{persons.map(person => (
											<option>{person.firstName}</option>
										))}
									</Select>
								</FormControl>
								<IconButton onClick={this.handleOpenCurrentPersonForm}>
									<AddIcon />
								</IconButton>
							</div>
						</Typography>
					)}
					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							Cancel
						</Button>
						{targetCoach ? (
							<>
								<Button
									style={{ margin: "10px" }}
									color="primary"
									variant="contained"
									onClick={this.deleteTargetCoach}
								>
									Delete
								</Button>
							</>
						) : (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.submitCurrentCoach}
							>
								Create
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default CoachForm;
