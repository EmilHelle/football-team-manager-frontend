import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Coach from "../../../model/Coach";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "coachStore", "rootView")
@observer
class CoachCard extends React.Component<any, any> {
	editCoach = (coach: Coach) => {
		console.log("editCoach=>");
		this.props.coachStore.setTarget(coach);
		this.props.rootView.coachView.setTargetFormOpen(true);
	};
	render() {
		let { coach } = this.props;
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;
		let {
			rootView: { theme }
		} = this.props;

		return (
			<Card style={{ 
				color: theme.currentTheme.textSecondary,
				backgroundColor: theme.currentTheme.mainLight, 
				maxWidth: "60%",
				minWidth: "450px", 
				margin: "10px" }}>
        		<CardContent >
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ color: theme.currentTheme.dark, float: "right" }} onClick={() => this.editCoach(coach)}>
							<EditIcon />
						</IconButton>
					)}
					{coach.person.firstName && (
						<Typography 
						variant="h6" 
						gutterBottom
						style={{color: theme.currentTheme.textMain}}>
								{coach.person.lastName ? coach.person.lastName : ""}
              					{coach.person.firstName ? (", " + coach.person.firstName) : ""}
						</Typography>
					)}
					<Divider/>
					{coach.person.dateOfBirth && (
						<Typography variant="body1" component="h2">
							<i>
								Born: {new Date(coach.person.dateOfBirth).getDate()}/
								{new Date(coach.person.dateOfBirth).getMonth()}/
								{new Date(coach.person.dateOfBirth).getFullYear()}
							</i>
						</Typography>
					)}
					{coach.person.address && (
						<div>
							<Typography
								variant="body2"
								style={{ margin: "5px 0 0 0" }}
								gutterBottom
							>
								<b>Address:</b>
								<br />
								{coach.person.address.addressLine1}
								{coach.person.address.addressLine2 ? [", " + coach.person.address.addressLine2] : ""}
								{coach.person.address.addressLine3 ? [", " + coach.person.address.addressLine3] : ""}
							</Typography>

							<Typography variant="body2" component="h2">
								{coach.person.address.postalCode}
							</Typography>
							<Typography variant="body2" component="h2">
								{coach.person.address.city}
							</Typography>
							<Typography variant="body2" component="h2">
								{coach.person.address.country}
							</Typography>
						</div>
					)}
				</CardContent>
			</Card>

		);
	}
}

export default CoachCard;
