import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import { FormControl, Select } from "@material-ui/core";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Address from "../../../../model/Address";
import Location from "../../../../model/Location";

@inject("rootView", "locationStore", "addressStore", "authStore")
@observer
class LocationForm extends React.Component<any, any> {
  componentDidMount() {
    this.props.addressStore.setList([]);
    this.getAddresses();
  }

  async getAddresses() {
    var returnedAddresses = await databaseAccessor.getAll("addresses", this.props.authStore.loggedInUser.token);
    for (let index = 0; index < returnedAddresses.length; index++) {
      const address = returnedAddresses[index];
      console.log("address", address);
      this.props.addressStore.add(
        new Address(
          address.postalCode,
          address.city,
          address.country,
          address.addressLine1,
          address.addressLine2,
          address.addressLine3,
          address.id
        )
      );
    }
  }


  handleFormClose = () => {
    let {
      rootView: {
        locationView: { setCurrentFormOpen, setTargetFormOpen }
      }
    } = this.props;
    let {
      locationStore: { target, current, setTarget, setCurrent }
    } = this.props;

    if (target) {
      setTargetFormOpen(false);
      setTarget(null);
    }
    if (target && current) {
      setCurrentFormOpen(false);
      setCurrent(new Location(new Address(0,'','',''),''));
    } else {
      setCurrentFormOpen(false);
      setCurrent(new Location(new Address(0,'','',''),''));
    }
  };
  
  handleOpenCurrentAddressForm = () => {
    this.props.rootView.addressView.setCurrentFormOpen(true);
  };
  
  handleSelectChangeAddress = e => {
    let targetValue = e.target.value;
    var address = this.props.addressStore.list.find(
      address => address.addressLine1 === targetValue
    );
    this.props.location.setAddress(address);
  };

  submitCurrentLocation = () => {
		databaseAccessor.create(
		  "locations",
      this.props.locationStore.current.locationToJson(),
      this.props.authStore.loggedInUser.token
    );
    this.props.locationStore.add(this.props.locationStore.current);
    this.props.locationStore.scrollToBottom();
    //close modal
    this.props.rootView.locationView.setCurrentFormOpen(false);
	  };
	
	  updateTargetLocation = () => {
		databaseAccessor.update(
		  "locations",
		  this.props.locationStore.target.locationToJson(),
      this.props.locationStore.target.id,
      this.props.authStore.loggedInUser.token
    );
     //close modal
	   this.props.rootView.locationView.setTargetFormOpen(false);
	  };
	
	  deleteTargetLocation = () => {
      databaseAccessor.delete(
        "locations",
        this.props.locationStore.target.locationToJson(),
        this.props.locationStore.target.id,
        this.props.authStore.loggedInUser.token
      );
      this.props.locationStore.remove(this.props.locationStore.target);
       //close modal
       this.props.rootView.locationView.setTargetFormOpen(false);
	  };

  render() {
    let { location } = this.props;
    let targetLocation = this.props.locationStore.target;
    let addresses = this.props.addressStore.list;

    return (
      <Modal
        style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
        open={true}
        onBackdropClick={this.handleFormClose}
        disableAutoFocus={true}
      >
        <Paper
          style={{
            width: "100%",
            height: "100%",
            padding: "20px",
            borderRadius: "15px"
          }}
        >
          <Typography style={{ width: "100%", margin: "10px" }} variant="h4">
            Location
          </Typography>

          <TextField
            required
            label="Name"
            variant="outlined"
            name="locationName"
            type="text"
            value={location.name}
            onChange={location.changeName}
            style={{ width: "45%", margin: "10px" }}
          />
          <TextField
            label="Description"
            variant="outlined"
            name="locationDescription"
            type="text"
            value={location.description}
            onChange={location.changeDescription}
            style={{ width: "45%", margin: "10px" }}
          />
          <Typography style={{ margin: "10px" }} variant="h6">
            Address
          </Typography>

          <FormControl variant="outlined">
            <Select
              native
              value="address"
              onChange={this.handleSelectChangeAddress}
              inputProps={{
                name: "address",
                id: "outlined-owner-native-simple"
              }}
            >
              {location.address ? (
                <option>{location.address.addressLine1}</option>
              ) : (
                <option value=""></option>
              )}
              {addresses &&
                addresses.map(address => <option>{address.addressLine1}</option>)}
            </Select>
          </FormControl>
          <IconButton onClick={this.handleOpenCurrentAddressForm}>
            <AddIcon />
          </IconButton>

          <div style={{ width: "100%" }}>
            <Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
              Cancel
            </Button>
            {targetLocation ? (
              <>
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.updateTargetLocation}
              >
                Update
              </Button>
              	<Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.deleteTargetLocation}
              >
                Delete
              </Button>
              </>
            ) : (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.submitCurrentLocation}
              >
                Create
              </Button>
            )}
          </div>
        </Paper>
      </Modal>
    );
  }
}

export default LocationForm;
