import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "locationStore", "rootView")
@observer
class LocationCard extends React.Component<any, any> {
  editLocation = (location: Location) => {
    console.log("editLocation=>");
    this.props.locationStore.setTarget(location);
    this.props.rootView.locationView.setTargetFormOpen(true);
  };
  render() {
    let { location } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;

    return (
			<Card style={{ 
				color: theme.currentTheme.textSecondary,
				backgroundColor: theme.currentTheme.mainLight, 
        maxWidth: "60%",
        minWidth: "450px", 
				margin: "10px" }}>
        <CardContent>
          {loggedInUser.user.isAdmin && (
            <IconButton
              style={{ float: "right" }}
              onClick={() => this.editLocation(location)}
            >
              <EditIcon />
            </IconButton>
          )}
          {location.name && (
            <Typography
              style={{ color: theme.currentTheme.textMain }}
              variant="h6"
              gutterBottom
            >
              <b>{location.name}</b>
            </Typography>
          )}
          <Divider />
          {location.description && (
            <Typography variant="body1" gutterBottom>
              <b>Description: </b>
              <i>{location.description}</i>
            </Typography>
          )}
          {location.address && (
            <div>
                <Typography
                variant="body2"
                style={{ margin: "5px 0 0 0" }}
                gutterBottom
              >
                <b>Address:</b>
                <br />
                {location.address.addressLine1}
                {location.address.addressLine2
                  ? [", " + location.address.addressLine2]
                  : ""}
                {location.address.addressLine3
                  ? [", " + location.address.addressLine3]
                  : ""}
              </Typography>
              <Typography variant="body2" component="h2">
                {location.address.postalCode}
              </Typography>
              <Typography variant="body2" component="h2">
                {location.address.city}
              </Typography>
              <Typography variant="body2" component="h2">
                {location.address.country}
              </Typography>
            </div>
          )}
        </CardContent>
      </Card>
    );
  }
}

export default LocationCard;
