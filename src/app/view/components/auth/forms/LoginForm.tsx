import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import { Modal, Divider } from "@material-ui/core";
import User from "../../../../model/User";

@inject("rootView", "userStore", "authStore")
@observer
class LoginForm extends React.Component<any, any> {
	handleFormClose = () => {
		let {
			rootView: {
				loginView: { setCurrentLoginFormOpen }
			}
		} = this.props;
		setCurrentLoginFormOpen(false);
	};

	handleLoginAttempt = () => {
		let {
			rootView: {
				loginView: { setCurrentLoginFormOpen }
			}
		} = this.props;
		let {
			authStore: { loggedInUser }
		} = this.props;
		let {
			userStore: { setCurrentUser}
		} = this.props;
		let {
			rootView: { setCurrentPage }
		} = this.props;

		setCurrentUser(new User("", "", ""));

		if (loggedInUser.token !== "" && loggedInUser.token !== undefined) {
			setCurrentLoginFormOpen(false);
			alert("Login successfull");
			setCurrentPage("matchPage");
		} else {
			alert("Invalid login");
		}			
	};

	render() {
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { login }
		} = this.props;

		return (
			<Modal
			style={{ display:"flex", alignItems:"center", justifyContent:"center"}}
			open={true}
			onBackdropClick={this.handleFormClose}
			disableAutoFocus={true}>	
				<Paper
					style={{
						width: "inherit",
						padding: "20px",
						borderRadius: "5px",
						margin: "10px 0 0 0"
					}}
				>
					<Typography style={{ margin: "10px" }} variant="h4">
						Login
					</Typography>
					<Divider/>
					<Typography style={{ margin: "10px" }} variant="body1">
						Username
					</Typography>
					<TextField
						variant="outlined"
						type="text"
						value={currentUser.name}
						onChange={currentUser.changeName}
						style={{ width:"300px", margin: "10px" }}
					/>
					<Typography style={{ margin: "10px" }} variant="body1">
						Password
					</Typography>
					<TextField
						variant="outlined"
						type="password"
						value={currentUser.password}
						onChange={currentUser.changePassword}
						style={{ width:"300px", margin: "10px" }}
					/>
					<div>
						<Button
							style={{ margin: "10px" }}
							color="primary"
							variant="contained"
							onClick={async () => { await login(currentUser); this.handleLoginAttempt();}}
						>
							Login
						</Button>
					</div>
				</Paper>
				</Modal>
		);
	}
}

export default LoginForm;
