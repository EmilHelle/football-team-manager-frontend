import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import { Modal } from "@material-ui/core";
import { observable } from "mobx";
import User from "../../../../model/User";

@inject("rootView", "userStore", "authStore")
@observer
class RegisterForm extends React.Component<any, any> {
	@observable errorLog = null;

	setErrorLog(errorLog) {
		this.errorLog = errorLog;
	}

	handleFormClose = () => {
		let {
			rootView: {
				loginView: { setCurrentRegisterFormOpen }
			}
		} = this.props;
		setCurrentRegisterFormOpen(false);
	};

	handleRegisterAttempt = () => {
		let {
			rootView: {
				loginView: { setCurrentRegisterFormOpen }
			}
		} = this.props;
		let {
			userStore: { setCurrentUser}
		} = this.props;

		setCurrentUser(new User("", "", ""));
		console.log("errorlog: ", this.errorLog)

		if (this.errorLog === undefined) {
			setCurrentRegisterFormOpen(false);
			alert("Registration successfull");
		} else {
			alert("Registration unsuccessfull");
		} 	
	};

	render() {
		let {
			userStore: { currentUser }
		} = this.props;
		let {
			authStore: { register }
		} = this.props;

		return (
			<Modal
			style={{ display:"flex", alignItems:"center", justifyContent:"center"}}
			open={true}
			onBackdropClick={this.handleFormClose}
			disableAutoFocus={true}>	
				<Paper
					style={{
						width: "inherit",
						padding: "20px",
						borderRadius: "5px",
						margin: "10px 0 0 0"
					}}
				>
					<Typography style={{ margin: "10px" }} variant="h4">
						Register Form
					</Typography>
					<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
						Username
					</Typography>
					<TextField
						variant="outlined"
						type="text"
						value={currentUser.name}
						onChange={currentUser.changeName}
						style={{ width:"300px", margin: "10px" }}
					/>
					<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
						Email
					</Typography>
					<TextField
						variant="outlined"
						type="email"
						value={currentUser.email}
						onChange={currentUser.changeEmail}
						style={{ width:"300px", margin: "10px" }}
					/>
					<Typography style={{ width: "50px", margin: "10px" }} variant="body1">
						Password
					</Typography>
					<TextField
						variant="outlined"
						name="password"
						type="password"
						value={currentUser.password}
						onChange={currentUser.changePassword}
						style={{ width:"300px", margin: "10px" }}
					/>
					<div>
						<Button
							style={{ margin: "10px" }}
							color="primary"
							variant="contained"
							onClick={async () => { this.setErrorLog(await register(currentUser)); this.handleRegisterAttempt();}}
						>
							Register
						</Button>
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default RegisterForm;
