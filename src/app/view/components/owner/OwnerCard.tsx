import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Owner from "../../../model/Owner";
import { Divider } from "@material-ui/core";


@inject("authStore", "userStore", "ownerStore", "rootView")
@observer
class OwnerCard extends React.Component<any, any> {
	editOwner = (owner: Owner) => {
		console.log("editOwner=>");
		this.props.ownerStore.setTarget(owner);
		this.props.rootView.ownerView.setTargetFormOpen(true);
	  };
	render() {
		let { owner } = this.props;
		let {
			userStore: { currentUser } } = this.props;
			let {
				authStore: { loggedInUser }
			} = this.props;
		let {
			rootView: { theme }
		} = this.props;

		return (

			<Card style={{ 
				color: theme.currentTheme.textSecondary,
				backgroundColor: theme.currentTheme.mainLight, 
				maxWidth: "60%",
				minWidth: "450px", 
				margin: "10px" }}>
        		<CardContent >
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ color: theme.currentTheme.dark, float: "right" }} onClick={() => this.editOwner(owner)}>
							<EditIcon />
						</IconButton>
					)}
					{owner.person.firstName && (
						<Typography 
						variant="h6" 
						gutterBottom
						style={{color: theme.currentTheme.textMain}}>
								{owner.person.lastName ? owner.person.lastName : ""}
              					{owner.person.firstName ? (", " + owner.person.firstName) : ""}
						</Typography>
					)}
					<Divider/>
					{owner.person.dateOfBirth && (
						<Typography variant="body1" component="h2">
							<i>
								Born: {new Date(owner.person.dateOfBirth).getDate()}/
								{new Date(owner.person.dateOfBirth).getMonth()}/
								{new Date(owner.person.dateOfBirth).getFullYear()}
							</i>
						</Typography>
					)}
					{owner.person.address && (
						<div>
							<Typography
								variant="body2"
								style={{ margin: "5px 0 0 0" }}
								gutterBottom
							>
								<b>Address:</b>
								<br />
								{owner.person.address.addressLine1}
								{owner.person.address.addressLine2 ? [", " + owner.person.address.addressLine2] : ""}
								{owner.person.address.addressLine3 ? [", " + owner.person.address.addressLine3] : ""}
							</Typography>

							<Typography variant="body2" component="h2">
								{owner.person.address.postalCode}
							</Typography>
							<Typography variant="body2" component="h2">
								{owner.person.address.city}
							</Typography>
							<Typography variant="body2" component="h2">
								{owner.person.address.country}
							</Typography>
						</div>
					)}
				</CardContent>
			</Card>

		);
	}
}

export default OwnerCard;
