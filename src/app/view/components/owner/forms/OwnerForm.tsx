import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Address from "../../../../model/Address";
import { IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Person from "../../../../model/Person";
import Owner from "../../../../model/Owner";

@inject("rootView", "ownerStore", "personStore", "authStore")
@observer
class OwnerForm extends React.Component<any, any> {
  componentDidMount() {
    this.props.personStore.setList([]);
    this.getPeople();
  }

  async getPeople() {
	var returnedPersons = await databaseAccessor.getAll('people', this.props.authStore.loggedInUser.token);
	for (let index = 0; index < returnedPersons.length; index++) {
		const person = returnedPersons[index];
			this.props.personStore.add(
				new Person(
					person.firstName,
					person.lastName,
					new Date(person.dateOfBirth),
					new Address(
						person.address.postalCode,
						person.address.city,
						person.address.country,
						person.address.addressLine1,
						person.address.addressLine2,
						person.address.addressLine3
					)
				)
			);
		}
	}

	handleFormClose = () => {
		let {
			rootView: {
				ownerView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			ownerStore: { target, current, setTarget, setCurrent }
		} = this.props;
		if (target) {
			setTargetFormOpen(false);
			setTarget(null);
		}
		if (target && current) {
			setCurrentFormOpen(false);
			setCurrent(new Owner(new Person("")));
		} else {
			setCurrentFormOpen(false);
			setCurrent(new Owner(new Person("")));
		}
	};

	handleOpenCurrentPersonForm = () => {
		this.props.rootView.personView.setCurrentFormOpen(true);
	};

	handleSelectChangePerson = e => {
		let targetValue = e.target.value;
		var person = this.props.personStore.list.find(person => person.firstName === targetValue);
		this.props.owner.setPerson(person);
	};

	submitCurrentOwner = () => {
		databaseAccessor.create(
		  "owners",
		this.props.ownerStore.current.ownerToJson(),
		this.props.authStore.loggedInUser.token
		);
		this.props.ownerStore.add(this.props.ownerStore.current);
		this.props.ownerStore.scrollToBottom();
		//close modal
		this.props.rootView.ownerView.setCurrentFormOpen(false);
	  };
	
	  updateTargetOwner = () => {
		databaseAccessor.update(
		  "owners",
		  this.props.ownerStore.target.ownerToJson(),
		this.props.ownerStore.target.id,
		this.props.authStore.loggedInUser.token
		);
		 //close modal
		 this.props.rootView.ownerView.setTargetFormOpen(false);
	  };
	
	  deleteTargetOwner = () => {
		databaseAccessor.delete(
		  "owners",
		  this.props.ownerStore.target.ownerToJson(),
		this.props.ownerStore.target.id,
		this.props.authStore.loggedInUser.token
		);
		this.props.ownerStore.remove(this.props.ownerStore.target);
		//close modal
		this.props.rootView.ownerView.setTargetFormOpen(false);
	  };

	render() {
		let { owner } = this.props;
		let targetOwner = this.props.ownerStore.target;
		let persons = this.props.personStore.list;

		return (
			<Modal
				style={{
					top: "10%",
					left: "10%",
					width: "75%",
					height: "75%",
					overflow: "auto"
				}}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "auto",
						height: "auto",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px" }} variant="h4">
						Owner
					</Typography>
					{targetOwner && (
						<Typography style={{ margin: "10px" }} variant="body1">
							An owner can only be deleted
						</Typography>
					)}
					{!targetOwner && (
						<>
							<Typography style={{ margin: "10px" }} variant="body1">
								Create a new owner
							</Typography>
							<FormControl variant="outlined">
								<Select
									native
									value="person"
									onChange={this.handleSelectChangePerson}
									inputProps={{
										name: "person",
										id: "outlined-address-native-simple"
									}}
								>
									{owner.person ? (
										<option>{owner.person.firstName}</option>
									) : (
										<option value=""></option>
									)}
									{persons.map(person => (
										<option>{person.firstName}</option>
									))}
								</Select>
							</FormControl>
							<IconButton onClick={this.handleOpenCurrentPersonForm}>
								<AddIcon />
							</IconButton>
						</>
					)}

					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							cancel
						</Button>
						{targetOwner ? (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.deleteTargetOwner}
							>
								Delete
							</Button>
						) : (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={this.submitCurrentOwner}
							>
								Create
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default OwnerForm;
