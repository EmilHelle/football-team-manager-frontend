import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Association from "../../../model/Association";
import { Divider } from "@material-ui/core";

@inject("authStore","userStore", "associationStore", "rootView")
@observer
class AssociationCard extends React.Component<any, any> {
  editAssociation = (association: Association) => {
    console.log("editAssociation=>");
    this.props.associationStore.setTarget(association);
    this.props.rootView.associationView.setTargetFormOpen(true);
  };

  render() {
    let { association } = this.props;
    let {
      userStore: { currentUser }
    } = this.props;
    let {
      authStore: { loggedInUser }
    } = this.props;
    let {
      rootView: { theme }
    } = this.props;

    return (
      <Card style={{ 
        color: theme.currentTheme.textSecondary,
        backgroundColor: theme.currentTheme.mainLight, 
        maxWidth: "60%",
				minWidth: "450px",
        margin: "10px" }}>
        <CardContent>
          {loggedInUser.user.isAdmin && (
            <IconButton
              style={{ float: "right" }}
              onClick={() => this.editAssociation(association)}
            >
              <EditIcon />
            </IconButton>
          )}
          {association.name && (
            <Typography 
            style={{color: theme.currentTheme.textMain}}
            variant="h6" 
            gutterBottom>
              {association.name}
            </Typography>
          )}
          <Divider/>
          {association.description && (
            <Typography gutterBottom>
              {association.description}
            </Typography>
          )}
        </CardContent>
      </Card>
    );
  }
}

export default AssociationCard;
