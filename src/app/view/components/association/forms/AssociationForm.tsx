import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";
import Association from "../../../../model/Association";

@inject("rootView", "associationStore", "authStore")
@observer
class AssociationForm extends React.Component<any, any> {
  handleFormClose = () => {
    let {
      rootView: {
        associationView: { setCurrentFormOpen, setTargetFormOpen }
      }
    } = this.props;
    let {
      associationStore: { target, current, setTarget, setCurrent }
    } = this.props;
    if (target) {
      setTargetFormOpen(false);
      setTarget(null);
    }
    if (target && current) {
      setCurrentFormOpen(false);
      setCurrent(new Association("", ""));
    } else {
      setCurrentFormOpen(false);
      setCurrent(new Association("", ""));
    }
  };

  submitCurrentAssociation = () => {
    databaseAccessor.create(
      "associations",
      this.props.associationStore.current.associationToJson(),
      this.props.authStore.loggedInUser.token
    );
    this.props.associationStore.add(this.props.associationStore.current);
    this.props.associationStore.scrollToBottom();
    //close modal
    this.props.rootView.associationView.setCurrentFormOpen(false);
  };

  updateTargetAssociation = () => {
    databaseAccessor.update(
      "associations",
      this.props.associationStore.target.associationToJson(),
      this.props.associationStore.target.id,
      this.props.authStore.loggedInUser.token
    );
    //close modal
    this.props.rootView.associationView.setTargetFormOpen(false);
  };

  deleteTargetAssociation = () => {
    databaseAccessor.delete(
      "associations",
      this.props.associationStore.target.associationToJson(),
      this.props.associationStore.target.id,
      this.props.authStore.loggedInUser.token
    );
    this.props.associationStore.remove(this.props.associatinoStore.target);
    //close modal
    this.props.rootView.associationView.setTargetFormOpen(false);
  };

  render() {
    let { association } = this.props;
    let {
      associationStore: { target }
    } = this.props;
    let targetAssociation = target;
    return (
      <Modal
        style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
        open={true}
        onBackdropClick={this.handleFormClose}
        disableAutoFocus={true}
      >
        <Paper
          style={{
            width: "100%",
            height: "100%",
            padding: "20px",
            borderRadius: "15px"
          }}
        >
          <Typography style={{ width: "100%", margin: "10px" }} variant="h4">
            Association
          </Typography>
          <TextField
            label="Name"
            variant="outlined"
            name="name"
            type="text"
            value={association.name}
            onChange={association.changeName}
            style={{ width: "50%", margin: "10px" }}
          />
          <TextField
            label="Description"
            variant="outlined"
            name="name"
            type="text"
            value={association.description}
            onChange={association.changeDescription}
            style={{ width: "50%", margin: "10px" }}
          />
          <div style={{ width: "100%" }}>
            <Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
              Cancel
            </Button>
            {targetAssociation ? (
              <>
                <Button
                  style={{ margin: "10px" }}
                  color="primary"
                  variant="contained"
                  onClick={this.updateTargetAssociation}
                >
                  Update
                </Button>
                <Button
                  style={{ margin: "10px" }}
                  color="primary"
                  variant="contained"
                  onClick={this.deleteTargetAssociation}
                >
                  Delete
                </Button>
              </>
            ) : (
              <Button
                style={{ margin: "10px" }}
                color="primary"
                variant="contained"
                onClick={this.submitCurrentAssociation}
              >
                Create
              </Button>
            )}
          </div>
        </Paper>
      </Modal>
    );
  }
}

export default AssociationForm;
