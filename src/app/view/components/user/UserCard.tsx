import React from "react";
import { inject, observer } from "mobx-react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import { Divider } from "@material-ui/core";

@inject("authStore", "userStore", "rootView")
@observer
class UserCard extends React.Component<any, any> {
  render() {
    let { user } = this.props;
    let {
      userStore: { editUser }
    } = this.props;
	let {
		authStore: { loggedInUser }
	} = this.props;
    let {
			rootView: { theme }
    } = this.props;

		return (
			<Card
				style={{
					backgroundColor: theme.currentTheme.mainLight,
					color: theme.currentTheme.textSecondary,
					maxWidth: "60%",
					minWidth: "450px",
					margin: "10px"
				}}
			>
				<CardContent>
					{loggedInUser.user.isAdmin && (
						<IconButton style={{ float: "right" }} onClick={() => editUser(user)}>
							<EditIcon />
						</IconButton>
					)}
					<Typography style={{color: theme.currentTheme.textMain}} variant="h6" gutterBottom>
						{user.isAdmin ? "Administrator user" : "Normal user"}
					</Typography>
					<Divider/>
					<Typography variant="h4" component="h3">
						{user.userName ? [" " + user.userName] : ""}
					</Typography>
					{user.email && (
						<div>
							<Typography variant="body2" style={{ margin: "5px 0 0 0" }} gutterBottom>
								<b>Name:</b>
								{user.name ? [" " + user.name] : ""}
							</Typography>
							<Typography variant="body2" style={{ margin: "5px 0 0 0" }} gutterBottom>
								<b>Email:</b>
								{user.email ? [" " + user.email] : ""}
							</Typography>
						</div>
					)}
				</CardContent>
			</Card>
		);
	}
}

export default UserCard;
