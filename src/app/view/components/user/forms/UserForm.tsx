import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { inject, observer } from "mobx-react";
import Modal from "@material-ui/core/Modal";
import databaseAccessor from "../../../../backend/data/DatabaseAccessor";

@inject("rootView", "userStore", "authStore")
@observer
class UserForm extends React.Component<any, any> {
	handleFormClose = () => {
		let {
			rootView: {
				userView: { setCurrentFormOpen, setTargetFormOpen }
			}
		} = this.props;
		let {
			userStore: { targetUser, currentUser, setTargetUser }
		} = this.props;
		if (targetUser) {
			setTargetFormOpen(false);
			setTargetUser(null);
		}
		if (targetUser && currentUser) {
			setCurrentFormOpen(false);
		} else {
			setCurrentFormOpen(false);
		}
	};

	updateTargetUser = () => {
		databaseAccessor.update(
			"users",
			this.props.userStore.target.userToJson(),
			this.props.userStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		//close modal
		this.props.rootView.userView.setTargetFormOpen(false);
	};

	deleteTargetUser = () => {
		databaseAccessor.delete(
			"users",
			this.props.userStore.target.userToJson(),
			this.props.userStore.target.id,
			this.props.authStore.loggedInUser.token
		);
		this.props.userStore.remove(this.props.userStore.target);
		//close modal
		this.props.rootView.userView.setTargetFormOpen(false);
	};

	render() {
		let { user } = this.props;
		let {
			userStore: { makeAdmin, deleteTargetUser, submitCurrentUser, updateTargetUser, targetUser }
		} = this.props;

		return (
			<Modal
				style={{ top: "10%", left: "10%", width: "75%", height: "75%" }}
				open={true}
				onBackdropClick={this.handleFormClose}
				disableAutoFocus={true}
			>
				<Paper
					style={{
						width: "100%",
						height: "100%",
						padding: "20px",
						borderRadius: "15px"
					}}
				>
					<Typography style={{ width: "100%", margin: "10px", inlineSize: "auto" }} variant="h4">
						User
					</Typography>
					<TextField
						required
						label="Name"
						variant="outlined"
						name="userName"
						value={user.name}
						style={{ width: "50%", margin: "10px" }}
					/>
					<TextField
						required
						label="Admin"
						variant="outlined"
						name="userType"
						value={user.isAdmin}
						onChange={user.changeType}
						style={{ width: "50%", margin: "10px" }}
					/>
					<TextField
						required
						label="Email"
						variant="outlined"
						name="userEmail"
						value={user.email}
						style={{ width: "50%", margin: "10px" }}
					/>
					<div style={{ width: "100%" }}>
						<Button style={{ margin: "10px" }} onClick={this.handleFormClose}>
							Cancel
						</Button>
						{targetUser && (
							<Button
								style={{ margin: "10px" }}
								color="primary"
								variant="contained"
								onClick={makeAdmin}
							>
								Make to admin
							</Button>
						)}
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default UserForm;
