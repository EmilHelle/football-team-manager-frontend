import axios from "axios";
import AuthStore from "../../state/stores/AuthStore";

class AuthenticationAccessor {
	private api: string;
	constructor() {
		this.api =
			"https://cors-anywhere.herokuapp.com/http://ftmapi.northeurope.azurecontainer.io/api/";
		console.log("creating new instance of AuthenticationAccessor");
	}

	loginUser(user: object) {
		try {
			const response = axios({
				method: "post",
				url: `${this.api}/authenticate/login`,
				data: JSON.stringify(user),
				headers: { "Content-Type": "application/json; charset=utf-8" }
			});
			return response;
		} catch (error) {
			console.log("Login error: ", error);
		}
	}

	async elevateToAdmin(adminObject: object, userObject: object, token: string) {
		var newAdmin = [adminObject, userObject];
		axios({
			method: "POST",
			url: `${this.api}/admin`,
			data: newAdmin,
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				Authorization: "bearer " + token
			}
		});
	}
	
	// logout(user:object){
	//     `logout`
	// }

	//   getAllUsers() {
	//     return axios.get(`${this.api}/users`);
	//   }
	register(user: object) {
		try {
			const response = axios({
				method: "post",
				url: `${this.api}/register/`,
				data: JSON.stringify(user),
				headers: { "Content-Type": "application/json; charset=utf-8" }
			});
			return response;
		} catch (error) {
			console.log("Register error: ", error);
		}
	}
}

// Export a singleton instance in the global namespace
const authenticationAccessor = new AuthenticationAccessor();

export default authenticationAccessor;
