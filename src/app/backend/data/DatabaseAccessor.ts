import axios from "axios";

class DatabaseAccessor {
	private api: string;
	constructor() {
		this.api =
			"https://cors-anywhere.herokuapp.com/http://ftmapi.northeurope.azurecontainer.io/api/";
		console.log("creating new instance of DatabaseAccessor");
	}

	async getAll(entities: string, token?: string): Promise<any[]> {
		try {
			const response = await axios({
				method: "GET",
				url: `${this.api}/${entities}`,
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					Authorization: "bearer " + token
				}
			});
			return response.data;
		} catch (error) {
			console.log(error);
			return [];
		}
	}

	async getSingle(entities: string, id1: number, token?: string, id2?: number) {
		try {
			if (!id2) {
				const response = await axios({
					method: "GET",
					url: `${this.api}/${entities}/${id1}`,
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						Authorization: "bearer " + token
					}
				});
				return response.data;
			} else {
				const response = await axios({
					method: "GET",
					url: `${this.api}/${entities}/${id1}/${id2}`,
					headers: {
						"Content-Type": "application/json; charset=utf-8",
						Authorization: "bearer " + token
					}
				});
				return response.data;
			}
		} catch (error) {
			console.log(error);
		}
	}

	async create(entities: string, jsonObject: object, token: string) {
			console.log("Creating new object in ", entities, " ", jsonObject);
			await axios({
				method: "post",
				url: `${this.api}/${entities}`,
				data: JSON.stringify(jsonObject),
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					Authorization: "bearer " + token
				}
			});
	}

	async delete(entities: string, jsonObject: object, id: number, token: string) {

			console.log("Trying to delete ", jsonObject, " from ", entities);
			await axios({
				method: "delete",
				url: `${this.api}/${entities}/${id}`,
				data: JSON.stringify(jsonObject),
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					Authorization: "bearer " + token
				}
			}).catch(function (error) {
				if (error.response) {
					console.log("Response status: ", error.response.status);
					return error.response.status;
				}
			})

	}

	async update(entities: string, jsonObject: object, id: number, token: string) {
		console.log("Trying to update ", entities, ": ", jsonObject);
		await axios({
			method: "put",
			url: `${this.api}/${entities}/${id}`,
			data: JSON.stringify(jsonObject),
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				Authorization: "bearer " + token
			}
		});
	}
}

// Export a singleton instance in the global namespace
const databaseAccessor = new DatabaseAccessor();

export default databaseAccessor;
