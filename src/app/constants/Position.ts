export enum Position {
    GOALKEEPER ="Goalkeeper",
    CENTREBACK ="Centre back", 
    SWEEPER =   "Sweeper",
    FULLBACK = "Full-back",
    WINGBACK = "Wing-back",
    CENTREMIDFIELD = "Centre midfield",
    DEFENSIVEMIDFIELD = "Defensive midfield",
    ATTACKINGMIDFIELD = "Attacking midfield", 
    WIDEMIDFIELD = "Wide midfield",
    CENTREFORWARD = "Centre forward",
    SECONDSTRIKER = "Second striker",
    WINGER = "Winger"
}