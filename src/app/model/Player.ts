import { observable, action } from "mobx";
import Team from "./Team";
import Person from "./Person";

class Player {
	readonly id?: number;
	@observable person?: Person;
	@observable team?: Team;
	@observable normalPosition?: string;
	@observable number?: number;

	constructor(person: Person, team: Team, normalPosition: string, number: number, id?: number) {
		this.person = person;
		this.team = team;
		this.normalPosition = normalPosition;
		this.number = number;
		this.id = id;
	}

	playerToJson() {
		return {
			id: this.id,
			person: this.person.personToJson(),
			team: this.team.teamToJson(),
			normalPosition: this.normalPosition,
			number: this.number
		};
	}

	playerCreateToJson() {
		return {
			person: this.person.personToJson(),
			team: this.team.teamToJson(),
			normalPosition: this.normalPosition,
			number: this.number
		};
	}

	@action.bound
	setTeam(team: Team): void {
		this.team = team;
	}

	@action.bound
	changeTeam(e): void {
		this.team = e.target.value;
	}

	@action.bound
	setNormalPosition(normalPosition: string): void {
		this.normalPosition = normalPosition;
	}

	@action.bound
	changeNormalPosition(e): void {
		this.normalPosition = e.target.value;
	}

	@action.bound
	setNumber(number: number): void {
		this.number = number;
	}

	@action.bound
	changeNumber(e): void {
		this.number = e.target.value;
	}

	@action.bound
	setPerson(person: Person) {
		this.person = person;
	}

	@action.bound
	changePerson(e) {
		this.person = e.target.value;
	}
}

export default Player;
