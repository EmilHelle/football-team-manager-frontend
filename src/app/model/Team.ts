import { observable, action } from "mobx";
import Player from "./Player";
import Coach from "./Coach";
import Owner from "./Owner";
import Association from "./Association";
import Location from "./Location";

class Team {
	readonly id?: number;
	@observable name: string;
	@observable owner?: Owner;
	@observable coach?: Coach;
	@observable association?: Association;
	@observable location?: Location;
	@observable players?: Player[];

	@observable coverImage?: string;
	@observable crestImage?: string;

	constructor(
		name: string, 
		owner?: Owner, 
		coach?: Coach, 
		association?: Association, 
		location?: Location,
		players?: Player[], 
		id?: number) {
			this.name = name;
			this.owner = owner;
			this.coach = coach;
			this.association = association;
			this.location = location;
			this.players = players;
			this.id = id;
	}

	teamToJson() {
		return {
			id: this.id,
			name: this.name,
			owner: this.owner.ownerToJson(),
			coach: this.coach.coachToJson(),
			association: this.association.associationToJson(),
			location: this.location.locationToJson(),
		};
	}

	@action.bound
	setName(name: string): void {
		this.name = name;
	}

	@action.bound
	changeName(e): void {
		this.name = e.target.value;
	}

	@action.bound
	setOwner(owner: Owner): void {
		this.owner = owner;
	}

	@action.bound
	changeOwner(e): void {
		this.owner = e.target.value;
	}

	@action.bound
	setCoach(coach: Coach): void {
		this.coach = coach;
	}

	@action.bound
	changeCoach(e): void {
		this.coach = e.target.value;
	}

	@action.bound
	setPlayers(players: Player[]): void {
		this.players = players;
	}

	@action.bound
	changePlayers(e): void {
		this.players = e.target.value;
	}

	@action.bound
	setAssociation(association: Association): void {
		this.association = association;
	}

	@action.bound
	changeAssociation(e): void {
		this.association = e.target.value;
	}

	@action.bound
	setLocation(location: Location): void {
		this.location = location;
	}

	@action.bound
	changeLocation(e): void {
		this.location = e.target.value;
	}

	@action.bound
	update() {}
}

export default Team;
