import { observable, action } from "mobx";
import databaseAccessor from "../backend/data/DatabaseAccessor";

class Goal {
	readonly id?: number;
	@observable typeOfGoal: string;

	constructor(typeOfGoal: string, id?: number) {
		this.typeOfGoal = typeOfGoal;
		this.id = id;
	}

	@action.bound
	setName(typeOfGoal: string): void {
		this.typeOfGoal = typeOfGoal;
	}

	@action.bound
	changeName(e): void {
		this.typeOfGoal = e.target.value;
	}

	goalToJson() {
		return {
			id: this.id,
			type: this.typeOfGoal
		};
	}
}

//Types of goal (shouldn't use this many, suggest using 3/5 types)
// The free-kick
// The header
// The penalty kick
// The counter-attack
// The chip or lob
// The long-range screamer
// The long-long-range goal
// The one with the great assist
// The overhead or scissor kick
// The solo goal
// The team goal
// The unique goal
// The volley

export default Goal;
