import { observable, action } from "mobx";

class User {
	readonly id?: number;
	@observable isAdmin?: boolean;
	@observable name: string;
	@observable email: string;
	@observable password: string;

	constructor(name: string, email: string, password: string, isAdmin?: boolean, id?: number) {
		this.isAdmin = isAdmin;
		this.name = name;
		this.email = email;
		this.id = id;
		this.password = password;
	}

	userToJson() {
		return {
			userName: this.name,
			email: this.email,
			password: this.password,
			isAdmin: this.isAdmin,
			id: this.id
		};
	}

	userLoginCredentialsToJson() {
		return {
			username: this.name,
			password: this.password
		};
	}

	@action.bound
	setName(name: string): void {
		this.name = name;
	}

	@action.bound
	changeName(e): void {
		this.name = e.target.value;
	}
	@action.bound
	setType(isAdmin: boolean): void {
		this.isAdmin = isAdmin;
	}

	@action.bound
	changeType(e): void {
		this.isAdmin = e.target.value;
	}
	@action.bound
	setEmail(email: string): void {
		this.email = email;
	}

	@action.bound
	changeEmail(e): void {
		this.email = e.target.value;
	}
	@action.bound
	changePassword(e): void {
		this.password = e.target.value;
	}
	@action.bound
	setPassword(password: string): void {
		this.password = password;
	}
}

export default User;
