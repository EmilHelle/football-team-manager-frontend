import { observable, action, computed } from "mobx";
import Person from "./Person";

class Owner {
	readonly id?: number;
	@observable person: Person;

	constructor(person: Person, id?: number) {
		this.person = person;
		this.id = id;
	}

	ownerToJson() {
		return {
			id: this.id,
			person: this.person.personToJson()
		};
	}

	@action.bound
	setPerson(person: Person): void {
		this.person = person;
	}

	@action.bound
	changePerson(e): void {
		this.person = e.target.value;
	}
}
export default Owner;
