import { observable, action } from "mobx";
import Team from "./Team";
import Match from "./Match";

class Result {
    readonly id?: number;
    @observable match: Match;
    @observable team: Team;
    @observable score: number;
    @observable matchResult: string;

	constructor(match: Match, team: Team, score: number, matchResult: string, id?: number) {
        this.match = match;
        this.team = team; 
        this.score = score; 
        this.matchResult = matchResult;
		this.id = id;
	}

	@action.bound
	setMatch(match: Match): void {
		this.match = match;
	}

	@action.bound
	changeMatch(e): void {
		this.match = e.target.value;
    }

    @action.bound
	setTeam(team: Team): void {
		this.team = team;
	}

	@action.bound
	changeTeam(e): void {
		this.team = e.target.value;
    }
    
    @action.bound
	setScore(score: number): void {
		this.score = score;
	}

	@action.bound
	changeScore(e): void {
		this.score = e.target.value;
    }
    
    @action.bound
	setResult(matchResult: string): void {
		this.matchResult = matchResult;
	}

	@action.bound
	changeResult(e): void {
		this.matchResult = e.target.value;
	}

	resultToJson() {
		return {
            match: this.match,
            team: this.team,
            score: this.score, 
            matchResult: this.matchResult
		};
	}
}


export default Result;
