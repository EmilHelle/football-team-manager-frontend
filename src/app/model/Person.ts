import { observable, action, computed } from "mobx";
import Address from "./Address";

class Person {
	readonly id?: number;
	@observable address?: Address;
	@observable firstName: string;
	@observable lastName?: string;
	@observable dateOfBirth?: Date;

	constructor(
		firstName: string,
		lastName?: string,
		dateOfBirth?: Date,
		address?: Address,
		id?: number
	) {
		this.id = id;
		this.address = address;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.id = id;
	}

	personToJson() {
		return {
			id: this.id,
			address: this.address.addressToJson(),
			firstName: this.firstName,
			lastName: this.lastName,
			dateOfBirth: this.dateOfBirth
		};
	}

	@computed
	get fullName(): string {
		return this.lastName + ", " + this.firstName;
	}

	@action.bound
	setAddress(address: Address): void {
		this.address = address;
	}

	@action.bound
	changeAddress(e): void {
		this.address = e.target.value;
	}

	@action.bound
	setFirstName(firstName: string): void {
		this.firstName = firstName;
	}

	@action.bound
	changeFirstName(e): void {
		this.firstName = e.target.value;
	}

	@action.bound
	setLastName(lastName: string): void {
		this.lastName = lastName;
	}

	@action.bound
	changeLastName(e): void {
		this.lastName = e.target.value;
	}

	@action.bound
	setDateOfBirth(dateOfBirth: Date): void {
		this.dateOfBirth = this.dateOfBirth;
	}

	@action.bound
	changeDateOfBirth(e): void {
		this.dateOfBirth = e.target.value;
	}
}

export default Person;
