import { observable, action, computed } from "mobx";

class Theme {
    @observable type:string;

    constructor(type:string) {
        this.type = type;
    }

    @computed
    get currentTheme(){
        if(this.type ==="dark"){
            //Setting dark background image
            document.body.style.background = "url('https://imgur.com/kx2ZPVu.jpg')  no-repeat center center fixed";
            document.body.style.backgroundSize ="cover";
            document.body.style.margin = "0";

            return  {
                secondary: "#686868A6",
                secondaryLight: "#929291A6",
                secondaryContrast: "white",
                main: "#00821e",
                mainLight: "#6f8f76A6",
                dark: "#2c3531",
                background: "#494738", 
                textMain: "white",
                textSecondary: "#ffffffa6",
                textSecondDark: "#505E58"
            }
        } else if(this.type === "light"){
            //Setting light background image
            document.body.style.background = "url('https://imgur.com/Sof0jIW.jpg')  no-repeat center center fixed";
            document.body.style.backgroundSize ="cover";
            document.body.style.margin = "0";

            return {
                secondary: "#9c9c9cA6",
                secondaryLight: "#545454A6",
                secondaryContrast: "white",
                main: "#00821e",
                mainLight: "#c8dbccA6",
                dark: "#2c3531",
                background: "#494738", 
                textMain: "black",
                textSecondary: "#00000073",
                textSecondDark: "#505E58"
            }    
        }
    }

    @action.bound
    toggleTheme() {
        if (this.type === "dark") {
            this.type = "light";
        } else {
            this.type = "dark"
        }
    }

}

export default Theme;