import { observable, action, computed } from "mobx";
import Person from "./Person";
import { ContactType } from "../constants/ContactType";
import Address from "./Address";

class Contact extends Person{
  readonly id?: number;
  @observable contactType: ContactType;
  @observable contactDetail: string; 

  constructor(		
    firstName: string,
		lastName: string,
		dateOfBirth: Date,
		address: Address,
    contactType: ContactType,
    contactDetail: string,
    id?: number
  ) {
      super(firstName, lastName, dateOfBirth, address);
      this.contactType = contactType;
      this.contactDetail = contactDetail;
      this.id = id;
  }


  contactToJson() {
    return {
      id: this.id,
      person: this.personToJson,
      contactType: this.contactType,
      contactDetail: this.contactDetail,
    }
  }

  @action.bound
  setContactType(contactType: ContactType): void {
    this.contactType = contactType;
  }

  @action.bound
  changeContactType(e): void {
    this.contactType = e.target.value;
  }

  @action.bound
  setContactDetail(contactDetail: string): void {
    this.contactDetail = contactDetail;
  }

  @action.bound
  changeContactDetail(e): void {
    this.contactDetail = e.target.value;
  }

  @action.bound
  update() {}
}

export default Contact;