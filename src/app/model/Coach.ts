import { observable, action } from "mobx";
import Person from "./Person";
import Address from "./Address";

class Coach {
	readonly id?: number;
	@observable person: Person;

	constructor(person: Person, id?: number) {
		this.person = person;
		this.id = id;
	}

	coachToJson() {
		return {
			id: this.id,
			person: this.person.personToJson()
		};
	}

	@action.bound
	setPerson(person: Person): void {
		this.person = person;
	}

	@action.bound
	changePerson(e): void {
		this.person = e.target.value;
	}
}
export default Coach;
