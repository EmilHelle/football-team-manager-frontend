import { observable, action, computed } from "mobx";

class Association {
	readonly id?: number;
	@observable name: string;
	@observable description: string;

	constructor(name: string, description: string, id?: number) {
		this.name = name;
		this.description = description;
		this.id = id;
	}

	@action.bound
	setName(name: string): void {
		this.name = name;
	}

	@action.bound
	changeName(e): void {
		this.name = e.target.value;
	}

	@action.bound
	setDescription(description: string): void {
		this.description = description;
	}

	@action.bound
	changeDescription(e): void {
		this.description = e.target.value;
	}
	@action.bound
	update() {}

	associationToJson() {
		return {
			name: this.name,
			description: this.description,
			id: this.id
		};
	}
}

export default Association;
