import { observable, action, computed } from "mobx";

class Season {
	readonly id?: number;
	@observable startDate: Date;
	@observable endDate: Date;
	@observable name: string;
	@observable description?: string; //Optional

	constructor(startDate: Date, endDate: Date, name: string, description?: string, id?: number) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.name = name;
		this.description = description;
		this.id = id;
	}

	seasonToJson() {
		return {
			id: this.id,
			startDate: this.startDate,
			endDate: this.endDate,
			name: this.name,
			seasonDescription: this.description
		};
	}

	@action.bound
	setStartDate(startDate: Date): void {
		this.startDate = startDate;
	}

	@action.bound
	changeStartDate(e): void {
		this.startDate = e.target.value;
	}
	@action.bound
	setEndDate(endDate: Date): void {
		this.endDate = endDate;
	}

	@action.bound
	changeEndDate(e): void {
		this.endDate = e.target.value;
	}

	@action.bound
	setName(name: string): void {
		this.name = name;
	}

	@action.bound
	changeName(e): void {
		this.name = e.target.value;
	}

	@action.bound
	setDescription(description: string): void {
		this.description = description;
	}

	@action.bound
	changeDescription(e): void {
		this.description = e.target.value;
	}

	@action.bound
	update() {}
}

export default Season;
