import { observable, action, computed } from "mobx";

class Address {
	readonly id?: number;
	@observable addressLine1: string;
	@observable addressLine2?: string; //Optional
	@observable addressLine3?: string; //Optional
	@observable postalCode: number;
	@observable city: string;
	@observable country: string;

	constructor(
		postalCode: number,
		city: string,
		country: string,
		addressLine1: string,
		addressLine2?: string,
		addressLine3?: string,
		id?: number
	) {
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.id = id;
	}

	addressToJson() {
		return {
			id: this.id,
			postalCode: this.postalCode,
			city: this.city,
			country: this.country,
			addressLine1: this.addressLine1,
			addressLine2: this.addressLine2,
			addressLine3: this.addressLine3
		};
	}

	@action.bound
	setAddressLine1(addressLine1: string): void {
		this.addressLine1 = addressLine1;
	}

	@action.bound
	changeAddressLine1(e): void {
		this.addressLine1 = e.target.value;
	}

	@action.bound
	setAddressLine2(addressLine2: string): void {
		this.addressLine2 = addressLine2;
	}

	@action.bound
	changeAddressLine2(e): void {
		this.addressLine2 = e.target.value;
	}

	@action.bound
	setAddressLine3(addressLine3: string): void {
		this.addressLine3 = addressLine3;
	}

	@action.bound
	changeAddressLine3(e): void {
		this.addressLine3 = e.target.value;
	}

	@action.bound
	setPostalCode(postalCode: number): void {
		this.postalCode = postalCode;
	}

	@action.bound
	changePostalCode(e): void {
		this.postalCode = e.target.value;
	}

	@action.bound
	setCity(city: string): void {
		this.city = city;
	}

	@action.bound
	changeCity(e): void {
		this.city = e.target.value;
	}

	@action.bound
	setCountry(country: string): void {
		this.country = country;
	}

	@action.bound
	changeCountry(e): void {
		this.country = e.target.value;
	}
}

export default Address;
