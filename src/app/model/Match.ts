import { observable, action, computed } from "mobx";
import Team from "./Team";
import Season from "./Season";
import Location from "./Location";

class Match {
	readonly id?: number;
	@observable date: Date;
	@observable homeTeam: Team;
	@observable awayTeam: Team;
	@observable season?: Season;
	@observable location?: Location;

	constructor(
		date: Date,
		homeTeam: Team,
		awayTeam: Team,
		season?: Season,
		location?: Location,
		id?: number
	) {
		this.date = date;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.season = season;
		this.location = location;
		this.id = id;
	}

	matchToJson() {
		return {
			date: this.date,
			homeTeam: this.homeTeam.teamToJson,
			awayTeam: this.awayTeam.teamToJson,
			season: this.season,
			location: this.location
		};
	}

	@computed
	get name(): string {
		return this.homeTeam.name + " (home) Vs. " + this.awayTeam.name + " (away)";
	}

	@action.bound
	setSeason(season: Season): void {
		this.season = season;
	}

	@action.bound
	changeSeason(e): void {
		this.season = e.target.value;
	}

	@action.bound
	setLocation(location: Location): void {
		this.location = location;
	}

	@action.bound
	changeLocation(e): void {
		this.location = e.target.value;
	}

	@action.bound
	setHomeTeam(team: Team) {
		this.homeTeam = team;
	}

	@action.bound
	setAwayTeam(team: Team) {
		this.awayTeam = team;
	}

	@action.bound
	setDate(date: Date): void {
		this.date = this.date;
	}

	@action.bound
	changeDate(e): void {
		this.date = e.target.value;
	}

	@action.bound
	update() {}
}

export default Match;
