import { observable, action, computed } from "mobx";
import Address from "./Address";

class Location {
	readonly id?: number;
	@observable address: Address;
	@observable name: string;
	@observable description?: string; //Optional

	constructor(address: Address, name: string, description?: string, id?: number) {
		this.address = address;
		this.name = name;
		this.description = description;
		this.id = id;
	}

	locationToJson() {
		return {
			id: this.id,
			address: this.address.addressToJson(),
			name: this.name,
			description: this.description
		};
	}

	@computed
	get locationName(): string {
		return this.name + ' - "' + this.description + '"';
	}

	@action.bound
	setAddress(address: Address): void {
		this.address = address;
	}

	@action.bound
	changeAddress(e): void {
		this.address = e.target.value;
	}

	@action.bound
	setName(name: string): void {
		this.name = name;
	}

	@action.bound
	changeName(e): void {
		this.name = e.target.value;
	}

	@action.bound
	setDescription(description: string): void {
		this.description = description;
	}

	@action.bound
	changeDescription(e): void {
		this.description = e.target.value;
	}

	@action.bound
	update() {}
}

export default Location;
