<h1 align="center">Welcome to Football Manager Frontend 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
</p>

> Frontend implementation of a football management system.

### ✨ [Demo](wwww.demoexample.firebaseapp.com)

## Install

```sh
npm install
```

## Usage

```sh
npm start
```

## Run tests

```sh
npm run test
```
## User manual

## User login
1) GUEST : Continue as a guest to see information available to guest type user
2) Register ->
Register form: 
a) Fill in username
b) Fill in email
c) Create password
d) Register
e) Go back to login and login by filling name and password
3) Admin : login by filling name and password

## DARK MODE
Edit mode
1) Move toggle to edit to either dark or light mode
 toggle on => dark mode
 toggle off => light mode
 
## ADMIN USER INSTRUCTIONS:

## PLAYERS: 
Add new player: 
1) Select person from person list
2) Give position
3) Give number
4) Select team from team list
5) Create

Edit player:
1) Open edit form
2) Edit position, write in the new position
3) Edit number, write in the new number
4) Edit team, choose new team from team list
5) Update

Delete player:
1) Open edit form
2) Delete

## OWNERS
Create new owner
1) Open create button
2) Select person from person list
3) Create

Delete owner
1) Open edit button
2) Delete

## COACHES
Create new coach
1) Open create button
2) Select person from person list
3) Create

Delete coach
1) Open edit button
2) Delete

## PERSONS
Create person
1) Open create button
2) Fill in first and last name
3) Choose date of birth
4) Select address from address list
5) Create

Edit person
1) Open edit button
2) Edit first and last name, write in new fist and last name
3) Edit date of birth, select new date of birth
4) Edit address, select new address from address list
5) Update

Delete person
1) Open edit button
2) Delete

## ASSOCIATIONS
Create new association
1) Open create button
2) Fill in name
3) Fill in description
4) Create

Edit association
1) Open edit button
2) Edit name, fill in new name
3) Edit description, fil in new description
4) Update

Delete association
1) Open edit button
2) Delete

## GOALS
Create goal
1) Open create button
2) Fill in type of goal
3) Create

Edit goal
1) Open edit button
2) Edit type, fill in new type of goal
3) Update

Delete goal
1) Open edit button
2) Delete

## SEASONS
Create season
1) Open edit button
2) Fill in start date
3) Fill in end date
4) Fill in season name
5) Fill in description
6) Create

Edit season
1) Open edit button
2) Edit start date, set new start date
3) Edit end date, set new end date
4) Edit name, fill in new name
5) Edit description, fill in new description
6) Update

Delete season
1) Open edit button
2) Delete

## PLAYERS
Create new player
1) Open create button
2) Choose a player from person list
3) Fill in position
4) Fill in number
5) Choose team, from team list
6) Create

Edit player
1) Open edit button
2) Edit position, fill in new position
3) Edit number, fill in new number
4) Edit team, choose new team from team list
5) Update

Delete player
1) Open edit button
2) Delete

## TEAMS
Create new team
1) Open create button
2) Fill in name
3) Choose owner from owner list
4) Choose coach from coach list
5) Choose association from association list
6) Choose location from location list
7) Create

Edit team
1) Open edit button 
2) Edit name, fill in new name
3) Edit owner, choose new owner from owner list
4) Edit coach, choose new coach from coach list
5) Edit association, choose new associaton from association list
6) Edit location, choose new location from location list
7) Update

Delete team
1) Open edit button
2) Delete 

## USERS
Edit normal user to admin
1) Open edit button
2) Click make to admin button

## MATCHES
Create match
1) Open create button
2) Choose home team, from team list
3) Choose away team, from team list
4) Create

Edit match
1) Open edit button
2) Edit home team, select new home team from team list
3) Edit away team, select new away team from team list
4) Update

Delete match
1) Open edit button
2) Delete

View more information about a match
1) Open match details
2) View details
3) Click match details again to close details

## ADDRESS
Create address
1) Open edit button
2) Fill in address 1
3) OPTIONAL: fill in address 2
4) OPTIONAL: fill in address 3
5) Fill in postal code
6) Fill in city
7) Fill in country
8) Create

Edit address
1) Open edit button
2) Edit address 1, fill in new address 1
3) Edit address 2, fill in new address 2 
4) Edit address 3, fill in new address 3
5) Edit city, fill in new city
6) Edit country, fill in new country
7) Update

Delete address
1) Open edit button
2) Delete

## EXAMPLES 
Login page
 <br />
    <img src="Images/login.png" alt="Logo" width="600" height="500">
  </a>

Seasons page
<br />
    <img src="Images/seasonHome.png" alt="Logo" width="600" height="400">
  </a>

Season create
<br />
    <img src="Images/seasonCreate.png" alt="Logo" width="600" height="400">
  </a>

Season edit<br />
    <img src="Images/seasonEdit.png" alt="Logo" width="600" height="400">
  </a>


## Author

👤 **Emil, Shanga, Sarah, Stian, Kenneth**

* Github: [@EmilHelle]

## Show your support

Give a ⭐️ if this project helped you!

***
_❤️_